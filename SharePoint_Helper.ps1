
#==============================================================
# Microsoft provides programming examples for illustration only, 
# without warranty either expressed or implied, including, but not 
# limited to, the implied warranties of merchantability and/or 
# fitness for a particular purpose. 
# This sample assumes that you are familiar with the programming 
# language being demonstrated and the tools used to create and debug 
# procedures. Microsoft support professionals can help explain the 
# functionality of a particular procedure, but they will not modify 
# these examples to provide added functionality or construct 
# procedures to meet your specific needs. if you have limited 
# programming experience, you may want to contact a Microsoft 
# Certified Partner or the Microsoft fee-based consulting line at 
# (800) 936-5200. 
# for more information about Microsoft Certified Partners, please 
# visit the following Microsoft Web site:
# https://partner.microsoft.com/global/30000104 
#
# Author: Tim Quinlan (tim.quinlan@microsoft.com)
#
# ----------------------------------------------------------
# History
# ----------------------------------------------------------


# ----------------------------------------------------------
#Executes a command on a specific computer using a set of credentials
# $computer:  The name of the computer the script will be run on.
# $cmd:  the command to be run
# $cred:  Set of credentials that will run the command.  These are only used when excuting on a remote computer.
# ----------------------------------------------------------
function Global:Invoke-CommandHelper
{
   param([string]$computer, [string]$cmd,[System.Management.Automation.PSCredential]$cred )

   $results=0;

   if( $computer -eq $env:computername)
   {
     #execute the command as is if running on a local computer
      & $cmd;
   }
   else
   {
     #format the command to be in the proper script block format
     try
     {
	     Write-Log -Level Info  -Message "Executing Block command: $($cmd)";
             $scriptBlock = GetScriptBlock -sb $cmd;
	     $rSession = New-PSSession -ComputerName $computer -Authentication Credssp -Credential $cred -Verbose;
	     Invoke-Command -Session $rSession -ScriptBlock $scriptBlock;
	     Remove-PSSession $rSession;

     }
     catch
     { 
         Write-Warning "An error occurred";
	 Write-Warning $_ #error details;
         $result = -1;
     }
 
   }
   return $results
  
}
# ----------------------------------------------------------
# Formats a command to scriptblock 
# $sb:  Command to be converted
# ----------------------------------------------------------
function Global:GetScriptBlock
{
  param([string]$sb)
  $scriptB = $executionContext.invokecommand.NewScriptBlock($sb);
  return $scriptB;

}

# ----------------------------------------------------------
# Configure remoting on a particular computer 
# $computerName:  Name of the computer
# $client:  True or False if the computer will be set up as a client (true) or server ( false)
# ----------------------------------------------------------
function Global:ConfigureRemoting
{
    param([string]$computerName,[switch]$client=$false)
   if( $computerName -eq $env:computername)
   {
     #Add Remoting and CredSSP
     Enable-PSRemoting -Force
	 #Set the Size of the memory allocated to the PowerShell window
     Set-Item WSMAN:\localhost\Shell\MaxMemoryPerShellMB 1000
     if($client)
     {
	#Enable CredSSP and allow delegation to any server in domain
        Enable-WSManCredSSP -Force -Role Client -DelegateComputer "*";
     }
     else
     {
	Enable-WSManCredSSP -Force -Role Server;
     }
   }
   else
   {
       Invoke-Command -ComputerName $computerName -ScriptBlock { Enable-PSRemoting -Force };
       Invoke-Command -ComputerName $computerName -ScriptBlock { Set-Item WSMAN:\localhost\Shell\MaxMemoryPerShellMB 1000 };
       if($client)
       {
       	Invoke-Command -ComputerName $computerName -ScriptBlock { Enable-WSManCredSSP -Force -Role Client -DelegateComputer "*"; };
          
       }
       else
       {
		  Invoke-Command -ComputerName $computerName -ScriptBlock { Enable-WSManCredSSP -Force -Role Server; };
       }	

   }
}
# ----------------------------------------------------------
# Tests a path to see if it exists, can create the path if it doesn't exist 
# $pathToTest:  The file path to check if it exists
# $computer: The computer to test the path on
# $force:  If the the path does not exists, create it
# ----------------------------------------------------------
function Global:Test-PathHelper
{
    param([string]$pathToTest, [string]$computer, [switch]$force)
    $results = $false;
    if( $computer -eq $env:Computername)
    {
    	if(Test-Path $pathToTest)
    	{
        	#Path exists
         	return $true;
    	}
	else
	{
	  if($force)
       	  {
          	New-Item -ItemType Directory -Path $pathToTest;
                $results  = Test-Path $pathToTest;
       	  }
        }
    else
    {
      #execute remotely
      $results = Test-PathRemotely -pathToTest $pathToTest -computer $computer -force $force
    }
    return $results;

}

} #end function Test-PathHelper

# ----------------------------------------------------------
# Tests a path to see if it exists, can create the path if it doesn't exist  on remote machines
# $pathToTest:  The file path to check if it exists
# $computer: The computer to test the path on
# $force:  If the the path does not exists, create it
# ----------------------------------------------------------
function Global:Test-PathRemotely
{
  param([string]$pathToTest, [string]$computer, [switch]$force)
  $exists = Invoke-CommandHelper -Comptuer $computer -cmd "Test-Path $pathToTest"
  if ($exists)
  {
     return $true;
  }
  else
  {
     $sb = "{ New-Item -Path $pathToTest -ItemType Directory }";
     Invoke-CommandHelper -Comptuer $computer -cmd $sb
     $exists = Invoke-CommandHelper -Comptuer $computer -cmd "Test-Path $pathToTest"
  }
  return $exists;

}

# ----------------------------------------------------------
# Creates a new registry key
# $keyPath:  The registry path to create the key at
# $computer: The computer to create the key on
# $keyName:  The name of the registry key to create
# ----------------------------------------------------------
function Global:New-RegistryKey
{
    param([string]$computer, [string]$keyPath, [string]$keyName)
    if($computer -eq $env:computername )
    {    
	   Write-Log -Level Info  -Message "Creating key $($keyName) to $($keyType) at $($keyPath) on $($computer) ";
       $currentValue = Get-Item -path "$($keyPath)\$($keyName)" -EA 0;
       
       if ( $currentValue -eq $null )
       {

         New-Item -Path $keyPath -Name $keyName -Force   | out-null
       }
    }
    else
    { 
	   Write-Log -Level Info  -Message "Creating key $($keyName) to $($keyType) at $($keyPath) on $($computer) ";
       $sb = { 
		param([string]$keyPath, [string]$keyName)
		$currentValue = Get-Item -path "$($keyPath)\$($keyName)" -EA 0;
       		if ( $currentValue -eq $null )
       		{
         		New-Item -Path $keyPath -Name $keyName -Force  | out-null
       		}
	      }
       Invoke-Command -Computer $computer -ScriptBlock $sb -ArgumentList $keyPath, $keyName;
     }
}#end function Set-RegistryKey

# ----------------------------------------------------------
# Set a registry entry value
# $keyPath:  The registry path to create the key at
# $computer: The computer to create the key on
# $keyName:  The name of the registry key to create
# $keyValue: The value to set
# $keyType:  The type of value to set: ( DWORD )
# ----------------------------------------------------------
function Global:Set-RegistryKey
{
    param([string]$computer, [string]$keyPath, [string]$keyName, [string]$keyValue, [string]$keyType)
    if($computer -eq $env:computername )
    {    
	   Write-Log -Level Info  -Message "Setting key $($keyName) to $($keyType) $($keyValue) at $($keyPath) on $($computer) ";
       $currentValue = Get-ItemProperty -path $keyPath -EA 0;
         New-ItemProperty -Path $keyPath -Name $keyName -Value $keyValue -PropertyType $keyType -Force   | out-null
       
    }
    else
    { 
	   Write-Log -Level Info  -Message "Setting key $($keyName) to $($keyType) $($keyValue) at $($keyPath) on $($computer) ";
       $sb = { 
		param([string]$keyPath, [string]$keyName, [string]$keyValue, [string]$keyType)
		$currentValue = Get-ItemProperty -path $keyPath -EA 0;
       		#if ( $currentValue -ne $null )
       		#{
         		New-ItemProperty -Path $keyPath -Name $keyName -Value $keyValue -PropertyType $keyType -Force | out-null    
       		#}
	      }
       Invoke-Command -Computer $computer -ScriptBlock $sb -ArgumentList $keyPath, $keyName,$keyValue, $keyType ;
     }
}#end function Set-RegistryKey

# ----------------------------------------------------------
# Creates Service Principal Name (SPN) for the specified account
# $spnValue:  The Protocol\Object format for the SPN
# $userAccount:  AD object to apply the SPN to.
# ----------------------------------------------------------
function Global:CreateSPN
{
  param([string]$spnValue, [string]$userAccount)
  Write-Log -Level Info  -Message "Creating SPN $($spnValue) on account $($userAccount)";
  & setspn -s $spnValue $userAccount

}


#adapted from work done by Joe Rodgers
# ----------------------------------------------------------
# Update IIS settings to point the files/web locations to the drive specified. For DoD, this needs to be non-system (c:) drive
# $server: The name ofthe server to set these settings on.
# $newDrive:  The new drive letter to place IIS on
# $credentails: Credentials set to apply to other servers.  Credentials musts be part of local administrators for any machine this is run on.
# ----------------------------------------------------------
function Global:Set-CustomIISSettings 

{
    param([string]$server, [string]$newDrive, [System.Management.Automation.PSCredential]$credentials)

    #make sure we are fomatted as expected
    $newDrive = $newDrive.TrimEnd("\")
    $newDrive = $newDrive + "\"
    $newLogPath = $newDrive.TrimEnd("\")
    $newLogPath = $newLogPath + "\"

    #timestamp for unique backup name
    $timestamp = Get-Date -uformat %m_%d_%Y.%H.%M.%S
    $backupName = "Backup_$timestamp" 

     # Ensure trailing
    $newInetpubPath = $newDrive + "inetpub\"
    $newInetpubPath = $newInetpubPath.TrimEnd("\")
    $newInetpubPath = $newInetpubPath + "\"
    $newConfigIsolationPath = $newInetpubPath + "temp\appPools\"
    $newWWWRootPath = $newInetpubPath + "wwwroot" #no trailing \
    $newFTPRootPath = $newInetpubPath + "ftproot" #no trailing \

    $newLogPath = $newLogPath.TrimEnd("\")
    $newLogPath = $newLogPath + "\"
    $newFailedReqLogPath = $iisLogPath + "\FailedReqLogFiles"
    $newIISLogPath       = $iisLogPath;
    $newConfigHistoryPath  = $newInetpubPath + "history"
    $newASPCachePath       = $newInetpubPath + "temp\ASP Compiled Templates"
    $newCompressedFilePath = $newInetpubPath + "temp\IIS Temporary Compressed Files"
    $newCustomErrorPath    = $newInetpubPath + "custerr"


	
    #Default settings

    $defaultInetputDir = "$env:systemdrive\inetpub\"
    $defaultFailedLogsDir = "$env:systemdrive\inetpub\logs\FailedReqLogFiles\"
    $defaultLogsDir = "$env:systemdrive\inetpub\logs\LogFiles\"


    Write-Log -Level Info  -Message "    Backing up IIS mestabase to $backupName"
    # execute a config backup before we do any config changes
    if( $server -ieq $env:computername)
    {
        &$env:WINDIR\system32\inetsrv\appcmd add backup $backupName > $null
    }
    else
    {
        $scriptBlock = { param($backupName) &$env:WINDIR\system32\inetsrv\appcmd add backup $backupName > $null }
        $remoteSession = New-PSSession -ComputerName $server -Credential $credentials -Authentication Credssp -Verbose
        Invoke-Command -Session $remoteSession -ScriptBlock $scriptBlock -ArgumentList $backupName
        Remove-PSSession $remoteSession
    } 

    Write-Log -Level Info  -Message "    Removing the Default Website"
    if( $server -ieq $env:computername)
    {
        $webAdminModule = Get-Module -ListAvailable | ? { $_.Name -ieq "WebAdministration" }
        if ($webAdminModule -ne $null) { Import-Module WebAdministration }
        else { Add-PSSnapin -Name WebAdministration }

        # Delete the Default Web Site, not needed for SharePoint
        $defaultWebSite = Get-Website -Name "Default WebSite" 

        if( $defaultWebSite)
        {
            Remove-Website "Default Web Site" -ErrorAction SilentlyContinue
        }
        if ($webAdminModule -ne $null) { Remove-Module WebAdministration }
        else {Remove-PSSnapin -Name WebAdministration -ErrorAction SilentlyContinue }
    }
    else
    {
        $scriptBlock = { 
            $webAdminModule = Get-Module -ListAvailable | ? { $_.Name -ieq "WebAdministration" }
            if ($webAdminModule -ne $null) { Import-Module WebAdministration }
            else { Add-PSSnapin -Name WebAdministration }
            $defaultWebSite = Get-Website -Name "Default WebSite" 

            if( $defaultWebSite)
            {
                Remove-Website "Default Web Site" -ErrorAction SilentlyContinue
            }
              if ($webAdminModule -ne $null) { Remove-Module WebAdministration }
            else {Remove-PSSnapin -Name WebAdministration -ErrorAction SilentlyContinue }
        }

        $remoteSession = New-PSSession -ComputerName $server -Credential $credentials -Authentication Credssp -Verbose
        Invoke-Command -Session $remoteSession -ScriptBlock $scriptBlock
        Remove-PSSession $remoteSession
    }


    Write-Log -Level Info  -Message "Stopping the World Wide Web Publishing Service "
    #Stop the World Wide Web Publishing Service 
    if( $server -ieq $env:computername)

    {
        Stop-Service iisadmin
    }
    else
    {
       $scriptBlock = { Stop-Service iisadmin }
        $remoteSession = New-PSSession -ComputerName $server -Credential $credentials -Authentication Credssp -Verbose
        Invoke-Command -Session $remoteSession -ScriptBlock $scriptBlock
        Remove-PSSession $remoteSession
    }
 

   
    if( $server -ieq $env:computername)
    {
        if( $defaultInetputDir -ne $newInetpubPath -and (Test-Path $defaultInetputDir) )
        {
            Write-Log -Level Info  -Message "    Copying $env:systemdrive\inetpub to $newInetpubPath"
            Copy-Item $defaultInetputDir -Destination $newInetpubPath -Recurse -Force
        }

        if( $defaultFailedLogsDir -ne $newFailedReqLogPath -and (Test-Path $defaultFailedLogsDir) )
        {
            Write-Log -Level Info  -Message "    Copying $env:systemdrive\inetpub\logs\FailedReqLogFiles to $newFailedReqLogPath"
            Copy-Item $defaultFailedLogsDir -Destination $newFailedReqLogPath -Recurse -Force
        }

        if( $defaultLogsDir -ne $newIISLogPath  -and (Test-Path $defaultLogsDir))
        {
            Write-Log -Level Info  -Message "    Copying $env:systemdrive\inetpub\logs\LogFiles to $newIISLogPath"
            Copy-Item $defaultLogsDir -Destination $newIISLogPath -Recurse -Force
        }
    }
    else
    {
        $remoteSession = New-PSSession -ComputerName $server -Credential $credentials -Authentication Credssp -Verbose    
        # I know this could be grouped together in one invoke cmd, but I wanted the logging to look uniform

        if( $defaultInetputDir -ne $newInetpubPath)
        {
            Write-Log -Level Info  -Message "    Copying $env:systemdrive\inetpub to $newInetpubPath"
            $scriptBlock = { 
                param([string]$newInetpubPath) 
                if (Test-Path $env:systemdrive\inetpub )
                {
                    Copy-Item $env:systemdrive\inetpub -Destination $newInetpubPath -Recurse -Force 
                }
            }
            Invoke-Command -Session $remoteSession -ScriptBlock $scriptBlock -ArgumentList $newInetpubPath
        }
        if( $defaultFailedLogsDir -ne $newFailedReqLogPath)
        {
            Write-Log -Level Info  -Message "    Copying $env:systemdrive\inetpub\logs\FailedReqLogFiles to $newFailedReqLogPath"
            $scriptBlock = { 
                param([string]$newFailedReqLogPath) 
                if (Test-Path $env:systemdrive\inetpub\logs\FailedReqLogFiles )
                {
                    Copy-Item $env:systemdrive\inetpub\logs\FailedReqLogFiles -Destination $newFailedReqLogPath -Recurse -Force
                }
            }
            Invoke-Command -Session $remoteSession -ScriptBlock $scriptBlock -ArgumentList $newFailedReqLogPath
        }
        if( $defaultLogsDir -ne $newIISLogPath)
        {
            Write-Log -Level Info  -Message "    Copying $env:systemdrive\inetpub\logs\LogFiles to $newIISLogPath"
            $scriptBlock = { 
                param([string]$newIISLogPath) 
                if( Test-Path $env:systemdrive\inetpub\logs\LogFiles )
                {
                    Copy-Item $env:systemdrive\inetpub\logs\LogFiles -Destination $newIISLogPath -Recurse -Force 
                }

            }
            Invoke-Command -Session $remoteSession -ScriptBlock $scriptBlock -ArgumentList $newIISLogPath
        }  
        Remove-PSSession $remoteSession
    }
    if( $server -ieq $env:computername)
    {
        Write-Log -Level Info  -Message "Setting HKLM:\System\CurrentControlSet\Services\WAS\Parameters\ConfigIsolationPath to $newConfigIsolationPath"

        # AppPool isolation is a new feature in IIS7. A dedicated AppPool configuration file gets automatically
        # created before a new Application Pool is started.  
        Set-ItemProperty -Path HKLM:\System\CurrentControlSet\Services\WAS\Parameters -Name ConfigIsolationPath -Value $newConfigIsolationPath

        Write-Log -Level Info  -Message "Setting HKLM:\Software\Microsoft\inetstp\PathWWWRoot to $newWWWRootPath"
        # Configure the PathWWWRoot reg key to point to the new wwwroot directory
        Set-ItemProperty -Path HKLM:\Software\Microsoft\inetstp -Name PathWWWRoot -Value $newWWWRootPath

        Write-Log -Level Info  -Message "    Setting HKLM:\Software\Microsoft\inetstp\PathFTPRoot to $newFTPRootPath"
        # Configure the PathFTPRoot reg key to point to the new ftproot directory
        Set-ItemProperty -Path HKLM:\Software\Microsoft\inetstp -Name PathFTPRoot -Value $newFTPRootPath
    }
    else
    {
        $remoteSession = New-PSSession -ComputerName $server -Credential $credentials -Authentication Credssp -Verbose       
        # I know this could be grouped together in one invoke cmd, but I wanted the logging to look uniform
        # AppPool isolation is a new feature in IIS7. A dedicated AppPool configuration file gets automatically
	# created before a new Application Pool is started.  

        Write-Log -Level Info  -Message "    Setting HKLM:\System\CurrentControlSet\Services\WAS\Parameters\ConfigIsolationPath to $newConfigIsolationPath"
        $scriptBlock = { param([string]$newConfigIsolationPath) Set-ItemProperty -Path HKLM:\System\CurrentControlSet\Services\WAS\Parameters -Name ConfigIsolationPath -Value $newConfigIsolationPath }
        Invoke-Command -Session $remoteSession -ScriptBlock $scriptBlock -ArgumentList $newConfigIsolationPath

    
        # Configure the PathWWWRoot reg key to point to the new wwwroot directory
        Write-Log -Level Info  -Message "    Setting HKLM:\Software\Microsoft\inetstp\PathWWWRoot to $newWWWRootPath"
        $scriptBlock = { param([string]$newWWWRootPath) Set-ItemProperty -Path HKLM:\Software\Microsoft\inetstp -Name PathWWWRoot -Value $newWWWRootPath }
        Invoke-Command -Session $remoteSession -ScriptBlock $scriptBlock -ArgumentList $newWWWRootPath

 
        # Configure the PathFTPRoot reg key to point to the new ftproot directory
        Write-Log -Level Info  -Message "    Setting HKLM:\Software\Microsoft\inetstp\PathFTPRoot to $newFTPRootPath"
        $scriptBlock = { param([string]$newFTPRootPath) Set-ItemProperty -Path HKLM:\Software\Microsoft\inetstp -Name PathFTPRoot -Value $newFTPRootPath }
        Invoke-Command -Session $remoteSession -ScriptBlock $scriptBlock -ArgumentList $newFTPRootPath

        Remove-PSSession $remoteSession
    }
    Write-Log -Level Info  -Message "Updating the IIS metabase with appcmd"

    if( $server -ieq $env:computername)
    {
        # Configure the trace logging of failed requests for a Web site.
        & $env:WINDIR\system32\inetsrv\appcmd set config "/section:system.applicationHost/sites" "/siteDefaults.traceFailedRequestsLogging.directory:$newFailedReqLogPath" "/commit:apphost" > $null
 
        # Configure default settings for handling and storage of log files for all sites.
        & $env:windir\system32\inetsrv\appcmd set config "/section:system.applicationHost/sites" "/siteDefaults.logfile.directory:$newIISLogPath" "/commit:apphost" > $null

        # Configure settings for centralized binary logging on a server
        & $env:WINDIR\system32\inetsrv\appcmd set config "/section:system.applicationHost/log" "/centralBinaryLogFile.directory:$newIISLogPath" "/commit:apphost" > $null

        # Configure settings for World Wide Web Consortium (W3C) centralized logging on a server.
        & $env:WINDIR\system32\inetsrv\appcmd set config "/section:system.applicationHost/log" "/centralW3CLogFile.directory:$newIISLogPath" "/commit:apphost" > $null  

        # Enable settings for returning to a well-known configuration state
        & $env:WINDIR\system32\inetsrv\appcmd set config "/section:system.applicationHost/configHistory" "/enabled:True" "/commit:apphost" "/commit:apphost" > $null

        # Configure storage path for returning to a well-known configuration state
        & $env:WINDIR\system32\inetsrv\appcmd set config "/section:system.applicationhost/configHistory" "/path:$newConfigHistoryPath" "/commit:apphost" > $null

        # Configure the directory that ASP uses to store compiled ASP templates to disk when the in-memory cache overflows
        & $env:WINDIR\system32\inetsrv\appcmd set config "/section:system.webServer/asp" "/cache.disktemplateCacheDirectory:$newASPCachePath" "/commit:apphost" > $null

        # Configure the directory where compressed versions of static files are temporarily stored and cached.
        & $env:WINDIR\system32\inetsrv\appcmd set config "/section:system.webServer/httpCompression" "/directory:$newCompressedFilePath" "/commit:apphost" > $null

        # Configure the directory where custom error pages are located.
        & $env:WINDIR\system32\inetsrv\appcmd set config "-section:httpErrors" "/[statusCode='401'].prefixLanguageFilePath:$newCustomErrorPath" "/commit:apphost" > $null
        & $env:WINDIR\system32\inetsrv\appcmd set config "-section:httpErrors" "/[statusCode='403'].prefixLanguageFilePath:$newCustomErrorPath" "/commit:apphost" > $null
        & $env:WINDIR\system32\inetsrv\appcmd set config "-section:httpErrors" "/[statusCode='404'].prefixLanguageFilePath:$newCustomErrorPath" "/commit:apphost" > $null
        & $env:WINDIR\system32\inetsrv\appcmd set config "-section:httpErrors" "/[statusCode='405'].prefixLanguageFilePath:$newCustomErrorPath" "/commit:apphost" > $null
        & $env:WINDIR\system32\inetsrv\appcmd set config "-section:httpErrors" "/[statusCode='406'].prefixLanguageFilePath:$newCustomErrorPath" "/commit:apphost" > $null
        & $env:WINDIR\system32\inetsrv\appcmd set config "-section:httpErrors" "/[statusCode='412'].prefixLanguageFilePath:$newCustomErrorPath" "/commit:apphost" > $null
        & $env:WINDIR\system32\inetsrv\appcmd set config "-section:httpErrors" "/[statusCode='500'].prefixLanguageFilePath:$newCustomErrorPath" "/commit:apphost" > $null
        & $env:WINDIR\system32\inetsrv\appcmd set config "-section:httpErrors" "/[statusCode='501'].prefixLanguageFilePath:$newCustomErrorPath" "/commit:apphost" > $null 
        & $env:WINDIR\system32\inetsrv\appcmd set config "-section:httpErrors" "/[statusCode='502'].prefixLanguageFilePath:$newCustomErrorPath" "/commit:apphost" > $null
    }
    else
    {
        $remoteSession = New-PSSession -ComputerName $server -Credential $credentials -Authentication Credssp -Verbose
        # I know this could be grouped together in one invoke cmd, but I wanted the logging to look uniform

        # Configure the trace logging of failed requests for a Web site.
        $scriptBlock = { param([string]$newFailedReqLogPath) & $env:WINDIR\system32\inetsrv\appcmd set config "/section:system.applicationHost/sites" "/siteDefaults.traceFailedRequestsLogging.directory:$newFailedReqLogPath" "/commit:apphost" > $null }
        Invoke-Command -Session $remoteSession -ScriptBlock $scriptBlock -ArgumentList $newFailedReqLogPath

        # Configure default settings for handling and storage of log files for all sites.
        $scriptBlock = { param([string]$newIISLogPath) & $env:windir\system32\inetsrv\appcmd set config "/section:system.applicationHost/sites" "/siteDefaults.logfile.directory:$newIISLogPath" "/commit:apphost" > $null}
        Invoke-Command -Session $remoteSession -ScriptBlock $scriptBlock -ArgumentList $newIISLogPath

        # Configure settings for centralized binary logging on a server
        $scriptBlock = { param([string]$newIISLogPath) & $env:WINDIR\system32\inetsrv\appcmd set config "/section:system.applicationHost/log" "/centralBinaryLogFile.directory:$newIISLogPath" "/commit:apphost" > $null}
        Invoke-Command -Session $remoteSession -ScriptBlock $scriptBlock -ArgumentList $newIISLogPath

        # Configure settings for World Wide Web Consortium (W3C) centralized logging on a server.
        $scriptBlock = { param([string]$newIISLogPath) & $env:WINDIR\system32\inetsrv\appcmd set config "/section:system.applicationHost/log" "/centralW3CLogFile.directory:$newIISLogPath" "/commit:apphost" > $null}
        Invoke-Command -Session $remoteSession -ScriptBlock $scriptBlock -ArgumentList $newIISLogPath       

        # Enable settings for returning to a well-known configuration state
        $scriptBlock = { & $env:WINDIR\system32\inetsrv\appcmd set config "/section:system.applicationHost/configHistory" "/enabled:True" "/commit:apphost" "/commit:apphost" > $null}
        Invoke-Command -Session $remoteSession -ScriptBlock $scriptBlock

        # Configure storage path for returning to a well-known configuration state
        $scriptBlock = { param([string]$newConfigHistoryPath) & $env:WINDIR\system32\inetsrv\appcmd set config "/section:system.applicationhost/configHistory" "/path:$newConfigHistoryPath" "/commit:apphost" > $null}
        Invoke-Command -Session $remoteSession -ScriptBlock $scriptBlock -ArgumentList $newConfigHistoryPath

        # Configure the directory that ASP uses to store compiled ASP templates to disk when the in-memory cache overflows
        $scriptBlock = { param([string]$newASPCachePath) & $env:WINDIR\system32\inetsrv\appcmd set config "/section:system.webServer/asp" "/cache.disktemplateCacheDirectory:$newASPCachePath" "/commit:apphost" > $null}
        Invoke-Command -Session $remoteSession -ScriptBlock $scriptBlock -ArgumentList $newASPCachePath

        # Configure the directory where compressed versions of static files are temporarily stored and cached.
        $scriptBlock = { param([string]$newCompressedFilePath) & $env:WINDIR\system32\inetsrv\appcmd set config "/section:system.webServer/httpCompression" "/directory:$newCompressedFilePath" "/commit:apphost" > $null}
        Invoke-Command -Session $remoteSession -ScriptBlock $scriptBlock -ArgumentList $newCompressedFilePath

        # Configure the directory where custom error pages are located.
        $scriptBlock = { param([string]$newCustomErrorPath) & $env:WINDIR\system32\inetsrv\appcmd set config "-section:httpErrors" "/[statusCode='401'].prefixLanguageFilePath:$newCustomErrorPath" "/commit:apphost" > $null}
        Invoke-Command -Session $remoteSession -ScriptBlock $scriptBlock -ArgumentList $newCustomErrorPath

        $scriptBlock = { param([string]$newCustomErrorPath) & $env:WINDIR\system32\inetsrv\appcmd set config "-section:httpErrors" "/[statusCode='403'].prefixLanguageFilePath:$newCustomErrorPath" "/commit:apphost" > $null}
        Invoke-Command -Session $remoteSession -ScriptBlock $scriptBlock -ArgumentList $newCustomErrorPath

        $scriptBlock = { param([string]$newCustomErrorPath) & $env:WINDIR\system32\inetsrv\appcmd set config "-section:httpErrors" "/[statusCode='404'].prefixLanguageFilePath:$newCustomErrorPath" "/commit:apphost" > $null}
        Invoke-Command -Session $remoteSession -ScriptBlock $scriptBlock -ArgumentList $newCustomErrorPath 

        $scriptBlock = { param([string]$newCustomErrorPath) & $env:WINDIR\system32\inetsrv\appcmd set config "-section:httpErrors" "/[statusCode='405'].prefixLanguageFilePath:$newCustomErrorPath" "/commit:apphost" > $null}
        Invoke-Command -Session $remoteSession -ScriptBlock $scriptBlock -ArgumentList $newCustomErrorPath

        $scriptBlock = { param([string]$newCustomErrorPath) & $env:WINDIR\system32\inetsrv\appcmd set config "-section:httpErrors" "/[statusCode='406'].prefixLanguageFilePath:$newCustomErrorPath" "/commit:apphost" > $null}
        Invoke-Command -Session $remoteSession -ScriptBlock $scriptBlock -ArgumentList $newCustomErrorPath

        $scriptBlock = { param([string]$newCustomErrorPath) & $env:WINDIR\system32\inetsrv\appcmd set config "-section:httpErrors" "/[statusCode='406'].prefixLanguageFilePath:$newCustomErrorPath" "/commit:apphost" > $null}
        Invoke-Command -Session $remoteSession -ScriptBlock $scriptBlock -ArgumentList $newCustomErrorPath

        $scriptBlock = { param([string]$newCustomErrorPath) & $env:WINDIR\system32\inetsrv\appcmd set config "-section:httpErrors" "/[statusCode='412'].prefixLanguageFilePath:$newCustomErrorPath" "/commit:apphost" > $null}
        Invoke-Command -Session $remoteSession -ScriptBlock $scriptBlock -ArgumentList $newCustomErrorPath

        $scriptBlock = { param([string]$newCustomErrorPath) & $env:WINDIR\system32\inetsrv\appcmd set config "-section:httpErrors" "/[statusCode='500'].prefixLanguageFilePath:$newCustomErrorPath" "/commit:apphost" > $null}
        Invoke-Command -Session $remoteSession -ScriptBlock $scriptBlock -ArgumentList $newCustomErrorPath

        $scriptBlock = { param([string]$newCustomErrorPath) & $env:WINDIR\system32\inetsrv\appcmd set config "-section:httpErrors" "/[statusCode='501'].prefixLanguageFilePath:$newCustomErrorPath" "/commit:apphost" > $null}
        Invoke-Command -Session $remoteSession -ScriptBlock $scriptBlock -ArgumentList $newCustomErrorPath
 
        $scriptBlock = { param([string]$newCustomErrorPath) & $env:WINDIR\system32\inetsrv\appcmd set config "-section:httpErrors" "/[statusCode='502'].prefixLanguageFilePath:$newCustomErrorPath" "/commit:apphost" > $null}
        Invoke-Command -Session $remoteSession -ScriptBlock $scriptBlock -ArgumentList $newCustomErrorPath
 
        Remove-PSSession $remoteSession
    }
 
    #Set any sites that had paths in the old directory
    Import-Module WebAdministration
    Write-Log -Level Info  -Message "Getting list of already provisioned web sites";
    $sitesToChange = Get-Website | ?{$_.PhysicalPath -like "c:\inetpub\wwwroot\wss*" }
    if($sitesToChange -ne $null)
    {
    $sitesToChange | %{
                        $newPath = $_.PhysicalPath.ToLower().Replace("c:", "d:" );
			Write-Log -Level Info  -Message "Replacing $($_.PhysicalPath) with $($newPath)";
			Set-ItemProperty "IIS:\Sites\$($_.Name)" -Name physicalPath -Value $newPath;
		       }
    }
    Write-Log -Level Info  -Message "    Starting the World Wide Web Publishing Service "
    #Start the World Wide Web Publishing Service 
    if( $server -ieq $env:computername)
    {
        Start-Service iisadmin
    }
    else
    {
        $scriptBlock = { Start-Service iisadmin }
        $remoteSession = New-PSSession -ComputerName $server -Credential $credentials -Authentication Credssp -Verbose
        Invoke-Command -Session $remoteSession -ScriptBlock $scriptBlock
        Remove-PSSession $remoteSession

    }
}

# ----------------------------------------------------------
# Install Certificate on a specific server
# $certPath: Path for the certificate file on disk
# $sslPassword: Password for the pfx file
# $server: Computer to install the certificate on
# $cred: Credential used to install certificates ( used when installing on a different computer )
# ----------------------------------------------------------
function Global:Install-SSLCertificates
{
  param([string]$certPath, [string]$sslPassword, [string]$server, [System.Management.Automation.PSCredential]$cred)

      #If installing locally, we don't need to do remoting
    if( $server -eq $env:computername )
    {
        $pfx = New-Object System.Security.Cryptography.X509Certificates.X509Certificate2 
        $pfx.Import($certPath, $sslPassword, "Exportable,PersistKeySet") 
      
        $certStore = New-Object System.Security.Cryptography.X509Certificates.X509Store(
        [System.Security.Cryptography.X509Certificates.StoreName]::My, 
        [System.Security.Cryptography.X509Certificates.StoreLocation]::LocalMachine ) 

        #Open the X.509 certificate store in Read/Write mode
        $certStore.Open([System.Security.Cryptography.X509Certificates.OpenFlags]::ReadWrite) 
        #Add the certificate to the X.509 certificate store
        $certStore.Add($pfx)     
        #Close the X.509 certificate store
        $certStore.Close() 
    }
    else
    {
        
		$copyPath = $certPath.Replace("D:", "\\$($env:computerName)\d$");
		Write-Log -Level Info  -Message "We will be installing certificates from $($copyPath)";
		#Copy-Item -Path $certPath -Destination $copyPath -Force

        $scriptBlock = { param([string]$copyPath, [string]$sslPassword) 
            $pfx = New-Object System.Security.Cryptography.X509Certificates.X509Certificate2      
            $pfx.Import($copyPath, $sslPassword, "Exportable,PersistKeySet") 
            #Initialize a new instance of the X509Store, using the personal store on the local machine
            $certStore = New-Object System.Security.Cryptography.X509Certificates.X509Store(
                [System.Security.Cryptography.X509Certificates.StoreName]::My, 
                [System.Security.Cryptography.X509Certificates.StoreLocation]::LocalMachine ) 
            #Open the X.509 certificate store in Read/Write mode
            $certStore.Open([System.Security.Cryptography.X509Certificates.OpenFlags]::ReadWrite) 
            #Add the certificate to the X.509 certificate store
            $certStore.Add($pfx)     
            #Close the X.509 certificate store
            $certStore.Close() 
         }
        $remoteSession = New-PSSession -ComputerName $server -Credential $cred -Authentication Credssp -Verbose
        Invoke-Command -Session $remoteSession -ScriptBlock $scriptBlock -ArgumentList $copyPath, $sslPassword
        Remove-PSSession $remoteSession

		Write-Log -Level Info  -Message "Removing certificate file from server"
		#Remove-Item -Path $copyPath
    }
}


# ----------------------------------------------------------
# Add SSL Binding to IIS web site to all listed in AIXConfiguration file
# $ip: IP Address for binding
# $certName: Name of the certificate for the binding
# $serverN: Name of the server to apply the binding to
# ----------------------------------------------------------
function Global:Add-SSLBinding
{
   param([string]$ip,[string]$certName,[string]$serverN)
   Write-Log -Level Info  -Message "Adding Certificate $($certName) to $($ip) on $($serverN)" ;
   if( $serverN -eq $env:computername )
    {
		 $webAdminModule = Get-Module -ListAvailable | ? { $_.Name -ieq "WebAdministration" }
			if ($webAdminModule -ne $null) { Import-Module WebAdministration }
			else { Add-PSSnapin -Name WebAdministration }
		Push-Location IIS:\SslBindings;
		$cert = (Get-ChildItem cert:\LocalMachine\My | where-object { $_.Subject -like "*$($certName)*" } | Select-Object -First 1);
		try
		{
			$cert | Set-Item $ip!443 -EA Stop;
		}
		catch
		{  
			$cert | New-Item $ip!443 -EA Stop;
			
		}
		Pop-Location;
	}
	else
	{
		$scriptBlock = {
			param([string]$ip,[string]$certName)
			 $webAdminModule = Get-Module -ListAvailable | ? { $_.Name -ieq "WebAdministration" }
			if ($webAdminModule -ne $null) { Import-Module WebAdministration }
			else { Add-PSSnapin -Name WebAdministration }
			Push-Location IIS:\SslBindings;
			$cert = (Get-ChildItem cert:\LocalMachine\My | where-object { $_.Subject -like "*$($certName)*" } | Select-Object -First 1);
			try
		{
			$cert | Set-Item $ip!443 -EA Stop;
		}
		catch
		{  
			$cert | New-Item $ip!443 -EA Stop;
			
		}
			Pop-Location;
		}
		Invoke-Command -ComputerName $serverN -scriptblock $scriptBlock -ArgumentList $ip,$certName;
	}
}

# ----------------------------------------------------------
# Add IP Binding to IIS web site to all listed in AIXConfiguration file
# $ip: IP Address for binding
# $webName: Name of the host header for the binding
# $serverN: Name of the server to apply the binding to
# ----------------------------------------------------------
function Global:Add-IPBinding
{
    param($ip,[string]$ssl,[string]$webName,[string]$serverN)
	Write-Log -Level Info  -Message "Adding IP Address $($ip) to $($webName) on $($serverN)" ;
	if( $serverN -eq $env:computername)
	{
		 $webAdminModule = Get-Module -ListAvailable | ? { $_.Name -ieq "WebAdministration" }
			if ($webAdminModule -ne $null) { Import-Module WebAdministration }
			else { Add-PSSnapin -Name WebAdministration }
		<#$bi = Get-WebBinding -Name $webName | ?{ $_.bindingInformation -like "*$($webName)*"} | select -expandproperty bindingInformation
		if( $bi -eq $null )
		{
		 $bi = Get-WebBinding -Name $webName | select -expandproperty bindingInformation
   		}
		Set-WebBinding -Name $webName -BindingInformation $bi -PropertyName IPAddress -Value $ip;#>
		Get-WebBinding -Name $webName | Remove-WebBinding
		if($ssl -eq "true")
		{
			New-WebBinding -Name $webName -Protocol https -Port 443 -IPAddress $ip;
		}
		else
		{
			New-WebBinding -Name $webName -Protocol http -Port 80 -IPAddress $ip -HostHeader $webName;
		}

	}
	else
	{
		$scriptBlock = {
		param($ip,[string]$ssl,[string]$webName)
		 	 $webAdminModule = Get-Module -ListAvailable | ? { $_.Name -ieq "WebAdministration" }
			if ($webAdminModule -ne $null) { Import-Module WebAdministration }
			else { Add-PSSnapin -Name WebAdministration }
			<#$bi = Get-WebBinding -Name $webName | ?{ $_.bindingInformation -like "*$($webName)*"} | select -expandproperty bindingInformation
		if( $bi -eq $null )
		{
		 $bi = Get-WebBinding -Name $webName | select -expandproperty bindingInformation
   		}
		Set-WebBinding -Name $webName -BindingInformation $bi -PropertyName IPAddress -Value $ip;#>
		Get-WebBinding -Name $webName | Remove-WebBinding
		if($ssl -eq "true")
		{
			New-WebBinding -Name $webName -Protocol https -Port 443 -IPAddress $ip;
		}
		else
		{
			New-WebBinding -Name $webName -Protocol http -Port 80 -IPAddress $ip -HostHeader $webName;
		}

		} #scriptblock
		Invoke-Command -ComputerName $serverN -scriptblock $scriptblock -ArgumentList $ip,$ssl,$webName
	}
}

# ----------------------------------------------------------
# Add IP Addresses to Machine
# $ips: Apply IP Addresses for the server
# $subnets: Array of subnets for server
# $serverN: Name of the server to apply the ip addresses
# ----------------------------------------------------------
function Global:Add-IPAddress
{
	param($ips,$subnets,[string]$serverN)
	Write-Log -Level Info  -Message "Adding IP Address(es) $($ips) to $($serverN)";
	if( $serverN -eq $env:computername)
	{
		# Add IP addresses to server
		$net = gwmi -class win32_networkadapterconfiguration | ?{$_.IPEnabled -match "TRUE"}
		$net.EnableStatic($ips,$subnets) 		
	}
	else
	{
		$scriptblock = {
			param($ips,$subnets)
			# Add IP addresses to server
			$net = gwmi -class win32_networkadapterconfiguration | ?{$_.IPEnabled -match "TRUE"}
			$net.EnableStatic($ips,$subnets) 	
		}
		Invoke-Command -ComputerName $serverN -scriptblock $scriptBlock -ArgumentList $ips,$subnets
	}
}
# ----------------------------------------------------------
# Write verify message to screen. If user specifies "n", then the script exists out.
# ----------------------------------------------------------
function Global:Verify
{ 
   
   $ans = Read-Host $vMessage
   
   	if( $ans -eq "n" )
   	{
	  
       	    exit;
   	}
	
}

# ----------------------------------------------------------
# Create SPN for Central Administration
# ----------------------------------------------------------
function Global:Create-SPN
{
    #Create SPNs for Kerberos on Central Administration
	$caServer = ($servers | ?{ $_.CA -eq $true} ).Name
	CreateSPN -spnValue "HTTP/$($caServer):$($caPort)" -userAccount $farmaccount;	
}

# ----------------------------------------------------------
# Create a database on SQL only
# $dbDataDefault: Path for the MDF file
# $dbLogDefault: Path for the LDF file
# $dbFileGrowth: Initial file growth for both MDF/LDF
# $farmaccount: Farm Account (domain\username)
# $databaseName: Name of the database
# ----------------------------------------------------------
function Global:Create-SPDatabaseOnSQL
{
	param([string]$dbDataDefault,[string]$dbLogDefault,[string]$dbfileGrowth,[string]$farmaccount,[string]$databaseName)
	#Copy Script to SQL Server
	if ( !(Test-Path "CreateSharePointDB.sql") )
	{
	Write-Log -Level Error -Message "Cannot locate CreateSharePointDB.sql file. Please copy this file into the same directory as this script"; 
	exit;
	}

	#Create the folder paths on the SQL server
	$dataPath = "\\$($activeClusterNode)\$($dbDataDefault.Replace(":","`$"))";
	$logPath = "\\$($activeClusterNode)\$($dbLogDefault.Replace(":","`$"))";
	Write-Log -Level Info  -Message "Creating directories at $($dataPath) and $($logPath)";
	New-Item -Path $dataPath -Type Directory -EA 0 -Force
	New-Item -Path $logPath -Type Directory -EA 0 -Force
	if( !(Test-Path "\\$($activeClusterNode)\c$\temp\") )
	{
	   New-Item -Path "\\$($activeClusterNode)\c$\temp\" -Type Directory -Force;
	}
	Copy-Item -Path CreateSharePointDB.sql -Destination "\\$($activeClusterNode)\c$\temp\" -force;
    Write-Log -Level Info  -Message "Creating database $($databaseName).." ;
	$sb = {
	 	param([string]$dbDataDefault,[string]$dbLogDefault,[string]$dbfileGrowth,[string]$farmaccount,[string]$databaseName,[string]$databaseServerName)
         	try
			{
			   add-pssnapin sqlserverprovidersnapin100;
			   add-pssnapin sqlservercmdletsnapin100;
			}catch{} 
	     	if($databaseName -ne "" )
 	     	{
				if( $dbfileGrowth -eq "" )
				{ $dbfileGrowth= "10MB"; }
	     		$param1="DBNAME="+ "$($databaseName)";
		     	$param2="DATAFILEPATH=" + $dbDataDefault;
		     	$param3="LOGFILEPATH=" + $dbLogDefault;
		     	$param4="FILEGROWTH=" + $dbfileGrowth;
		     	$param5="SETUPUSER=" + $farmaccount;
			    $param6="RECOVERYMODE=SIMPLE";
		     	$params = $param1, $param2, $param3, $param4,$param5,$param6;
	 	     	
			
		     	Invoke-SQLCmd -InputFile "C:\temp\CreateSharePointDB.sql" -Variable $params -ServerInstance $databaseServerName;
	        }
	} #end of script block
	#exectue for CONTENT DB
	Invoke-Command -ComputerName $activeClusterNode -ScriptBlock $sb -ArgumentList $dbDataDefault,$dbLogDefault,$dbfileGrowth,$farmaccount,$databaseName,$databaseServerName

}

# ----------------------------------------------------------
# Check if SharePoint 2013 is installed
# ----------------------------------------------------------
function Global:Test-SharePointServer
{
   $MyInvocation.InvocationName
   $spPath = "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\Office15.OSERVER"
   $results = $true;
   Write-Log -Level Info  -Message "Checking if SharePoint Server 2013 is installed:  ";
   switch( Test-Path $spPath)
   {
      $true{ Write-Log -Level Info  -Message "Passed" ;  }
      $false{ Write-Log -Level Error  -Message "Failed"; $results=$false; }

   }
   
   return $results

}

# ----------------------------------------------------------
# Add a 
# ----------------------------------------------------------
function Global:Add-ServiceApplicationPermission
{
   param([string]$userName,[string]$permission,[string]$serviceAppName,[switch]$admin)
   
           $principal = New-SPClaimsPrincipal $userName -IdentityType WindowsSamAccountName;
	   $spapp = Get-SPServiceApplication �Name $serviceAppName;
	   if( $admin)
	   {
	   	$security = Get-SPServiceApplicationSecurity $spapp -Admin;
	   	Grant-SPObjectSecurity $security $principal $permission;
	   	Set-SPServiceApplicationSecurity $spapp $security -Admin;
           }
	   else
	   {
		$security = Get-SPServiceApplicationSecurity $spapp;
	   	Grant-SPObjectSecurity $security $principal $permission;
	   	Set-SPServiceApplicationSecurity $spapp $security;
	   }
}

# ----------------------------------------------------------
# Create SPN for Central Administration
# ----------------------------------------------------------
function Global:Add-WebApplicationPolicy
{
   param([string]$userName,[string]$displayName,[string]$role,[Microsoft.SharePoint.PowerShell.SPWebApplicationPipeBind]$webapplication)

   $wa = Get-SPWebApplication $webapplication;
   if ( $wa -ne $null )
   {
   $policy = $wa.Policies.Add($userName,$displayName);
   $policy.PolicyRoleBindings.Add($wa.PolicyRoles.GetSpecialRole($role));
   $wa.Update();
   }
   else
   {
      Write-Log -Level Error -Message "No web application was found";
	}

}

# ----------------------------------------------------------
# Create SPN for Central Administration
# ----------------------------------------------------------
function Global:Add-PermissionLevel
{
   param([string]$name,[string]$mask,[string]$description)
   $rep = Read-Host "Do you want to apply this permission level: $($name) to all webs ?(y/n)";
   if($rep -eq "y")
   {
	   Get-SPSite -limit all | %{ 
		   $web = $_.OpenWeb();
		   if($web.RoleDefinitions["$($name)"] -eq $null )
		   {
			 Write-Log -Level Info  -Message "Creating $($name) Permission Level at $($web.Url)";
			 $newRole = new-object Microsoft.SharePoint.SPRoleDefinition;
			 $newRole.BasePermissions = $mask;
			 $newRole.Name = $name;
			 $newRole.Description = $description;
			 $web.RoleDefinitions.Add($newRole); 
			 $web.Dispose();
		  }
	   }
   }
}

# ----------------------------------------------------------
# Create SPN for Central Administration
# ----------------------------------------------------------
function Global:Update-PermissionLevel
{
   param([string]$oldName,[string]$newName,[string]$mask,[string]$description)
   $rep = Read-Host "Do you want to update this permission level: $($oldName) to all webs ?(y/n)";
   if($rep -eq "y")
   {
	   Get-SPSite -limit all | %{ 
		   $web = $_.OpenWeb();
		   if($web.RoleDefinitions["$($oldName)"] -ne $null )
		   {
			 Write-Log -Level Info  -Message "Creating $($name) Permission Level at $($web.Url)";
			 $newRole = $web.RoleDefinitions["$($oldName)"];
			 $newRole.BasePermissions = $mask;
			 $newRole.Name = $newName;
			 $newRole.Description = $description;
			 $newRole.Update();
			 $web.Dispose();
		  }
	   }
   }
}

# ----------------------------------------------------------
# Create SPN for Central Administration
# ----------------------------------------------------------
function Enable-SPBlobCache { 
	param( 
		[Parameter(Mandatory=$true, ValueFromPipeline=$true, Position=0)] [Microsoft.SharePoint.PowerShell.SPWebApplicationPipeBind] $WebApplication ) 
process { 

	    ## SPWebConfigModificationType.EnsureChildNode -> 0 
		## SPWebConfigModificationType.EnsureAttribute -> 1 
		## SPWebConfigModificationType.EnsureSection -> 2 

    $WebApp = $WebApplication.Read() 
    # SPWebConfigModification to enable BlobCache 
    $configMod1 = New-Object Microsoft.SharePoint.Administration.SPWebConfigModification 
    $configMod1.Path = "configuration/SharePoint/BlobCache" 
    $configMod1.Name = "enabled" 
    $configMod1.Sequence = 0 
    $configMod1.Owner = "BlobCacheMod" 
    $configMod1.Type = 1 
    $configMod1.Value = "true" 
    # SPWebConfigModification to enable client-side Blob caching (max-age) 
    $configMod2 = New-Object Microsoft.SharePoint.Administration.SPWebConfigModification 
    $configMod2.Path = "configuration/SharePoint/BlobCache" 
    $configMod2.Name = "max-age" 
    $configMod2.Sequence = 0 
    $configMod2.Owner = "BlobCacheMod" 
    $configMod2.Type = 1 
    $configMod2.Value = "86400" 
	#Modification to change path;
	$configMod3 = New-Object Microsoft.SharePoint.Administration.SPWebConfigModification 
    $configMod3.Path = "configuration/SharePoint/BlobCache" 
    $configMod3.Name = "location" 
    $configMod3.Sequence = 0 
    $configMod3.Owner = "BlobCacheMod" 
    $configMod3.Type = 1 
    $configMod3.Value = $blobCachePath;

    # Add mods, update, and apply 
    $WebApp.WebConfigModifications.Add( $configMod1 ) ;
    $WebApp.WebConfigModifications.Add( $configMod2 ) ;
	$WebApp.WebConfigModifications.Add( $configMod3 ) ;
    $WebApp.Update() ;
    $WebApp.Parent.ApplyWebConfigModifications(); 
	Start-sleep 30;
  } 
} 

# ----------------------------------------------------------
# Create SPN for Central Administration
# ----------------------------------------------------------
function Enable-ASPSession
{
   param(
   	[Parameter(Mandatory=$true, ValueFromPipeline=$true, Position=0)] [Microsoft.SharePoint.PowerShell.SPWebApplicationPipeBind] $WebApplication ) 
	process { 
	    
	    $WebApp = $WebApplication.Read() 
		Write-Log -Level Info -Message "Enabling Session State in Web.Config file for $($WebApp.Url)";
		# SPWebConfigModification to enable BlobCache 
		$configMod1 = New-Object Microsoft.SharePoint.Administration.SPWebConfigModification 
		$configMod1.Path = "configuration/system.web/pages" 
		$configMod1.Name = "enableSessionState" 
		$configMod1.Sequence = 0 
		$configMod1.Owner = "SessionStateMod" 
		$configMod1.Type = 1 
		$configMod1.Value = "true" 
		$WebApp.WebConfigModifications.Add( $configMod1 );
		$WebApp.Update() ;
		$WebApp.Parent.ApplyWebConfigModifications(); 
		Start-sleep 30;
	}
}

# ----------------------------------------------------------
# Create SPN for Central Administration
# ----------------------------------------------------------
Function CreateObjectCacheAccountSettings {
    $superReaderPropertyString = "portalsuperreaderaccount"
    $superUserPropertyString = "portalsuperuseraccount"
    $FullReadRoleName = "Full Read"
    $FullControlRoleName = "Full Control"
 
    # DA Snippet for Cache Accounts
    Get-SPWebApplication | %{
 
                   $Zone = $_.IISSettings.Item("Default")
                                             
                   if($Zone.UseClaimsAuthentication -eq $True){
                                  $SuperUserPrincipal = New-SPClaimsPrincipal -Identity $SuperUserAccount -IdentityType WindowsSamAccountName
                                  $SuperUserAccountEncoded = $SuperUserPrincipal.ToEncodedString()
 
                                  $superReaderPrincipal = New-SPClaimsPrincipal -Identity $SuperReaderAccount -IdentityType WindowsSamAccountName
                                  $SuperReaderAccountEncoded = $superReaderPrincipal.ToEncodedString()
                   }
 
                   $SuperReaderPolicy = $_.Policies | WHERE {$_.DisplayName -eq "Object Cache Super Reader"}
                   if ($SuperReaderPolicy -eq $Null){
                                  $SuperReaderPolicy = $_.Policies.Add($SuperReaderAccountEncoded, "Object Cache Super Reader")
                   }
                   $Role = $_.PolicyRoles | where {$_.Name -like $FullReadRoleName}
 
                                             
                   $SuperReaderPolicy.PolicyRoleBindings.Add($Role)
                   $_.Properties[$superReaderPropertyString] = [System.String]$SuperReaderAccountEncoded
 
                   $SuperUserPolicy= $_.Policies | WHERE {$_.DisplayName -eq "Object Cache Super User"}
                   if ($SuperUserPolicy -eq $Null){
                                  $SuperUserPolicy = $_.Policies.Add($SuperUserAccountEncoded, "Object Cache Super User")
                   }
 
                   $Role = $_.PolicyRoles | where {$_.Name -like $fullControlRoleName}
                   $SuperUserPolicy.PolicyRoleBindings.Add($Role)
 
                   $_.Properties[$superUserPropertyString] = [System.String]$SuperUserAccountEncoded
                   $_.Update()
    }
 
}

# ----------------------------------------------------------
# Create SPN for Central Administration
# ----------------------------------------------------------
function Configure-ADFS
{
	$rootPath = "$($pwd.Path)\$($adfsRootCertPath)";
	$signPath = "$($pwd.Path)\$($adfsSignCertPath)";
	$secondRootPath = "$($pwd.Path)\$($adfsSecondaryRootCertPath)";
 
	$root = New-Object System.Security.Cryptography.X509Certificates.X509Certificate2($rootPath);
	$root2 = New-Object System.Security.Cryptography.X509Certificates.X509Certificate2($secondRootPath);
	$cert = New-Object System.Security.Cryptography.X509Certificates.X509Certificate2($signpath);
 
	New-SPTrustedRootAuthority -Name "$($adfsName) Root Certificate" -Certificate $root
	New-SPTrustedRootAuthority -Name "$($adfsName) Root Certificate Secondary" -Certificate $root2
	New-SPTrustedRootAuthority -Name "$($adfsName)Token Signing Certificate" -Certificate $cert
 
	$emailClaimMap = New-SPClaimTypeMapping -IncomingClaimType "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress" -IncomingClaimTypeDisplayName "EmailAddress" -SameAsIncoming
	$upnClaimMap = New-SPClaimTypeMapping -IncomingClaimType "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/upn" -IncomingClaimTypeDisplayName "UPN" -SameAsIncoming
	$roleClaimMap = New-SPClaimTypeMapping -IncomingClaimType "http://schemas.microsoft.com/ws/2008/06/identity/claims/role" -IncomingClaimTypeDisplayName "Role" -SameAsIncoming
	$sidClaimMap = New-SPClaimTypeMapping -IncomingClaimType "http://schemas.microsoft.com/ws/2008/06/identity/claims/primarysid" -IncomingClaimTypeDisplayName "SID" -SameAsIncoming
 
	$realm = $adfsUrn;
	$signInURL = $adfsSignInUrl;
 
	$ap = New-SPTrustedIdentityTokenIssuer -Name "$($adfsName)" -Description "$($adfsName) Server" `
	-realm $realm -ImportTrustCertificate $cert `
	-ClaimsMappings $emailClaimMap,$upnClaimMap,$roleClaimMap,$sidClaimMap `
	-SignInUrl $signInURL -IdentifierClaim $emailClaimmap.InputClaimType


}

# ----------------------------------------------------------
# Create SPN for Central Administration
# ----------------------------------------------------------
function Deploy-RobotsFile
{
    param([string]$server)
	#Get all the paths to copy to
	$dirTemp = "\\$($server)\d`$";
	$vPaths = (Get-SPWebApplication | %{ $_.IIsSettings | %{ $_.Values.Path.FullName } }) -replace "d:", $dirTemp;

	$vPaths | %{
	  
		$filepath = $_ + "\robots.txt"
		$serverTag = "#$($server)";

		Write-Log -Level Info  -Message ("Creating File At: " + $filepath)
		$serverTag | out-file $filepath -Encoding ASCII;
		"User-agent: Mozilla/4.0 (compatible; MSIE 4.01; Windows NT; MS Search 6.0 Robot)" | out-file $filepath -append -Encoding ASCII;
		"Allow: /" | out-file $filepath -append -Encoding ASCII;
		"User-agent: Googlebot" | out-file $filepath -append -Encoding ASCII;
		"Disallow: /" | out-file $filepath -append -Encoding ASCII;
		"User-agent: *" | out-file $filepath -append -Encoding ASCII;
		"Disallow: /" | out-file $filepath -append -Encoding ASCII;

		try {
			$acl = Get-Acl $filepath;
			$rule = New-Object System.Security.AccessControl.FileSystemAccessRule ("Everyone","Read","Allow");
			$rule2 = New-Object System.Security.AccessControl.FileSystemAccessRule ("NT AUTHORITY\Anonymous Logon","Read","Allow");

			$rule = New-Object System.Security.AccessControl.FileSystemAccessRule ("Everyone","ReadAndExecute","Allow");
			$rule2 = New-Object System.Security.AccessControl.FileSystemAccessRule ("NT AUTHORITY\Anonymous Logon","ReadAndExecute","Allow");
			$acl.SetAccessRule($rule);
			$acl.SetAccessRule($rule2);
			$acl | Set-Acl -Path $filepath;
		}
		catch {
			write-Log -Level Error -Message "Error setting permissions";
		}

	}#each path
}

# ----------------------------------------------------------
# Create SPN for Central Administration
# ----------------------------------------------------------
function Create-UPASyncConnections
{
   Write-Log -Level Info -Message "Please enter the user name and password for the profile synchronization account.";
   $pCred = Get-Credential;
   $connUserName  = $pCred.UserName;
   $pDomain = $connUserName.Split("\")[0];
   $pName = $connUserName.Split("\")[1];
   $upa = Get-SPServiceApplication | ?{ $_.DisplayName -eq "$($userProfileSAName)" };
   $syncConnections | %{

		Write-Log -Level Info  -Message "Creating Connection for $($_.Forest) to $($_.OUPath) with user $($pName) in domain $($pDomain)";
		Add-SPProfileSyncConnection -ProfileServiceApplication $upa -ConnectionDomain $pDomain -ConnectionForestName $_.Forest -ConnectionPassword $pCred.Password `
		-ConnectionSynchronizationOU $_.OUPath -ConnectionUserName $pName
   }
}

# ----------------------------------------------------------
# Create SPN for Central Administration
# ----------------------------------------------------------
###############################
#  From PoshCode.org
###############################
function Write-Log {

	#region Parameters
	
		[cmdletbinding()]
		Param(
			[Parameter(ValueFromPipeline=$true,Mandatory=$true)] [ValidateNotNullOrEmpty()]
			[string] $Message,

			[Parameter()] [ValidateSet("Error", "Warn", "Info")]
			[string] $Level = "Info",
			

			[Parameter()]
			[string] $Path = "SharePoint_Install_Log.log",
			
			[Parameter()]
			[Switch] $Clobber,
			
			[Parameter()]
			[String] $EventLogName,
			
			[Parameter()]
			[String] $EventSource = ($MyInvocation.ScriptName).Name,
			
			[Parameter()]
			[Int32] $EventID = 1
			
		)
		
	#endregion

	Begin {}

	Process {
		try {			
			$calling = $MyInvocation.ScriptName;
			$calling = $calling.Substring($calling.LastIndexOf("\") +1, ($calling.Length - $calling.LastIndexOf("\")) -1 );
			$msg = '{0} {1}{2} : {3} : {4}' -f $calling," ", (Get-Date -Format "yyyy-MM-dd HH:mm:ss"), $Level.ToUpper(), $Message
			
			
			switch ($Level) {
				'Error' { Write-Host ('ERROR: {0}{1}{2}' -f " ", $Message,$error[0]) -ForegroundColor Red -BackgroundColor black}
				'Warn' { Write-Host ('WARNING: {0}{1}' -f " ", $Message) -ForegroundColor Yellow -BackgroundColor black}
				'Info' { Write-Host ('INFO: {0}{1}' -f " ", $Message) -ForegroundColor Green -BackgroundColor black}
			}
			$errormsg = "";
			if($error.Count -gt 0)
			{
			   $errormsg = '{0} {1}{2} : {3} : {4}' -f "$($calling):$($error[0].InvocationInfo.ScriptLineNumber)", " ", (Get-Date -Format "yyyy-MM-dd HH:mm:ss"), "Error", $error[0];
			   $error.Clear();
			}
			
			if ($Clobber) {
			    if( $errormsg -ne "" )
				{
					$errormsg | Out-File  $Path
				}
				$msg | Out-File  $Path
			} else {
				if( $errormsg -ne "" )
				{
					$errormsg | Out-File  $Path -Append
				}
				$msg | Out-File $Path -Append
			}
			
			if ($EventLogName) {
			
				if(-not [Diagnostics.EventLog]::SourceExists($EventSource)) { 
					[Diagnostics.EventLog]::CreateEventSource($EventSource, $EventLogName) 
		        } 

				$log = New-Object System.Diagnostics.EventLog  
			    $log.set_log($EventLogName)  
			    $log.set_source($EventSource) 
				
				switch ($Level) {
					"Error" { $log.WriteEntry($Message, 'Error', $EventID) }
					"Warn"  { $log.WriteEntry($Message, 'Warning', $EventID) }
					"Info"  { $log.WriteEntry($Message, 'Information', $EventID) }
				}
			}

		} catch {
			throw "Failed to create log entry in: �$Path�. The error was: �$_�."
		}
	}

	End {}

	<#
		.SYNOPSIS
			Writes logging information to screen and log file simultaneously.

		.DESCRIPTION
			Writes logging information to screen and log file simultaneously. Supports multiple log levels.

		.PARAMETER Message
			The message to be logged.

		.PARAMETER Level
			The type of message to be logged.
		
		.PARAMETER Indent
			The number of spaces to indent the line in the log file.

		.PARAMETER Path
			The log file path.
		
		.PARAMETER Clobber
			Existing log file is deleted when this is specified.
		
		.PARAMETER EventLogName
			The name of the system event log, e.g. 'Application'.
		
		.PARAMETER EventSource
			The name to appear as the source attribute for the system event log entry. This is ignored unless 'EventLogName' is specified.
		
		.PARAMETER EventID
			The ID to appear as the event ID attribute for the system event log entry. This is ignored unless 'EventLogName' is specified.

		.EXAMPLE
			PS C:\> Write-Log -Message "It's all good!" -Path C:\MyLog.log  -EventLogName 'Application'

		.EXAMPLE
			PS C:\> Write-Log -Message "Oops, not so good!" -Level Error -EventID 3 -Indent 2 -EventLogName 'Application' -EventSource "My Script"

		.INPUTS
			System.String

		.OUTPUTS
			No output.
	#>
}

#============================================================================================#
#							Permission Functions		     #
#============================================================================================#

# ----------------------------------------------------------
# Create SPN for Central Administration
# ----------------------------------------------------------
function Global:Add-AnonymousAccessToWeb()
{
	param([Microsoft.SharePoint.PowerShell.SPWebPipeBind]$web)
	$currentWeb = $web.Read();
	Write-Log -Level Info  -Message "Adding anonymouse access to $($currentWeb.Url)" 
	$currentWeb.AnonymousState = 2;
	$currentWeb.AnonymousPermMask64 = "ViewListItems, ViewVersions, ViewFormPages, Open, ViewPages, OpenItems, UseClientIntegration";
	$currentWeb.Update();
	$currentWeb.Dispose();
}

# ----------------------------------------------------------
# Create SPN for Central Administration
# ----------------------------------------------------------
function Global:Add-UsersToSiteGroups()
{
   param([Microsoft.SharePoint.PowerShell.SPWebPipeBind]$web,[string]$userLogin,[string]$groupName,[switch]$isGroup)
   $currentWeb = $web.Read();
   #format group name
   $sg = $currentWeb.SiteGroups[$groupName];
   $sg.Name;
   Write-Log -Level Info  -Message "Attempting to add User: $($userLogin) to Site Group $($groupName) to $($currentWeb.Url)" 
   if($sg -ne $null )
   { 
      #get principal format
	  if($isGroup)
	  {
	    try
	    {
	    	$prin = (New-SPClaimsPrincipal -Identity $userLogin -IdentityType WindowsSecurityGroupName -EA Stop).ToEncodedString();
	    }
	    catch
	    {
		Write-Log -Level Error -Message "An error occurred converting $($userLogin) to claims principal";
	    }
	  }
	  else
	  {
	    try
	    {
	    	$prin = (New-SPClaimsPrincipal -Identity $userLogin -IdentityType WindowsSamaccountname -EA Stop).ToEncodedString();
	    }
	    catch
	    {
		Write-Log -Level Error -Message "An error occurred converting $($userLogin) to claims principal";
	    }
	  }
	  #add user
	  if( $prin -ne $null )
	  {
	  	
	  	if(($currentWeb.SiteUsers[$prin] -eq $null) )
	  	{
 	     		$currentWeb.SiteUsers.Add($prin,"",$userLogin,"");
	  	}
	  	if( $sg.Users[$prin] -eq $null)
	  	{
	         	Write-Log -Level Info  -Message "Adding User: $($userLogin) to Site Group $($groupName) to $($currentWeb.Url)" 	 
		 	$sg.AddUser($prin,"",$userLogin,"");
	  	}
	  	else
	  	{
			Write-Log -Level Warn -Message "$($userLogin) is already a member of the group $($groupName)";
	  	}
	  }
   }
   else
   {
		Write-Log -Level Error -Message "The group $($groupName) does not exist";
   }
   $currentWeb.Update();
   $currentWeb.Dispose();
}

# ----------------------------------------------------------
# Create SPN for Central Administration
# ----------------------------------------------------------
function Add-SiteGroups()
{
   param([Microsoft.SharePoint.PowerShell.SPWebPipeBind]$web,[string]$groupName)
   
   #Add site group
   $currentWeb = $web.Read();
   Write-Log -Level Info  -Message "Checking if Site Group $($groupName) exits on $($currentWeb.Url)" 
   if($currentWeb.SiteGroups[$groupName] -eq $null )
   { 
      Write-Log -Level Info  -Message "Adding Site Group $($groupName) to $($currentWeb.Url)" 
      #group does not exists, so add it
	  $ow = (New-SPClaimsPrincipal -Identity $farmaccount -IdentityType WindowsSamAccountName).ToEncodedString()
      $owner = $currentWeb.AllUsers[$ow];
	  if( $owner -eq $null )
	  {
	     $owner = $currentWeb.AllUsers.Add($ow, "", $farmaccount,"");
	  }
	 
      $currentWeb.SiteGroups.Add($groupName, $owner, $null, "");
      $currentWeb.Update();
      
   }
   else
   {
	Write-Log -Level Warn -Message "Site Group $($groupName) already exits on $($currentWeb.Url)" 
   } 

   $currentWeb.Dispose();
}

# ----------------------------------------------------------
# Create SPN for Central Administration
# ----------------------------------------------------------
function Add-PermissionToWeb
{
  param([Microsoft.SharePoint.PowerShell.SPWebPipeBind]$web,[string]$groupName,[string]$mask)
    $currentWeb = $web.Read();
    Write-Log -Level Info  -Message "Creating permission $($mask) for $($groupName) at $($currentWeb.Url)";
            $grp = $currentWeb.SiteGroups[$groupName];
            if ($grp -ne $null)
            {
				$roleAssignment = $currentWeb.RoleAssignments | ?{ $_.Member.Name -eq $groupName};
				if($roleAssignment -eq $null )
				{
					$roleAssignment = new-object Microsoft.SharePoint.SPRoleAssignment($grp);
					$roleDef = $currentWeb.RoleDefinitions[$mask];
					$roleAssignment.RoleDefinitionBindings.Add($roleDef);
					$currentWeb.RoleAssignments.Add($roleAssignment);
					$currentWeb.Update();
				}
				else
				{
					$roleDef = $currentWeb.RoleDefinitions[$mask];
					$roleAssignment.RoleDefinitionBindings.RemoveAll();
					$roleAssignment.RoleDefinitionBindings.Add($roleDef);
					$roleAssignment.Update();
				}
            }
    $currentWeb.Dispose();
}
function Add-PermissionToLibrary
{
  param([Microsoft.SharePoint.PowerShell.SPWebPipeBind]$web,[string]$groupName,[string]$mask,[string]$listName)
    $currentWeb = $web.Read();
    Write-Log -Level Info  -Message "Creating permission $($mask) for $($groupName) at $($currentWeb.Url) for $($list)";
            $grp = $currentWeb.SiteGroups[$groupName];
            if ($grp -ne $null)
            {
		Write-Host "1";
				$list = $currentWeb.Lists[$listName];

				if( $list -ne $null )
				{
					$roleAssignment = $list.RoleAssignments | ?{ $_.Member.Name -eq $groupName};
					if($roleAssignment -eq $null )
					{
						$roleAssignment = new-object Microsoft.SharePoint.SPRoleAssignment($grp);
						$roleDef = $currentWeb.RoleDefinitions[$mask];
						$roleAssignment.RoleDefinitionBindings.Add($roleDef);
						$list.RoleAssignments.Add($roleAssignment);
						$list.Update();
					}
					else
					{
						$roleDef = $currentWeb.RoleDefinitions[$mask];
						$roleAssignment.RoleDefinitionBindings.RemoveAll();
						$roleAssignment.RoleDefinitionBindings.Add($roleDef);
						$roleAssignment.Update();
					}
				}
            }
    $currentWeb.Dispose();
}
# ----------------------------------------------------------
# Create SPN for Central Administration
# ----------------------------------------------------------
function Global:Add-DocumentLibrary
{
   param([Microsoft.SharePoint.PowerShell.SPWebPipeBind]$web,[string]$internalName,[string]$title)
   $currentWeb = $web.Read();
   $list = $currentWeb.Lists | ?{ ($_.Title -eq $title) -and ($_.DefaultViewUrl -like "*$($internalName)*") }
   if( $list -eq $null)
   {
   if($currentWeb.Features["bba312fb-1d35-45bf-8cb8-2ac50ecb32ba"] )
   {     
    Write-Log -Level Info -Message "Adding Document Library $($internalName) to $($currentWeb.Url)";
   	$listTemplate = $currentWeb.ListTemplates["Document Library"]; 
   	$currentWeb.Lists.Add($internalName,"",$listTemplate); 
   	$list = $currentWeb.Lists[$internalName]; 
   	$list.Title = $title;
	$list.OnQuickLaunch = $true;
   	$list.Update();
   	
   }
   else
   {
 	Write-Log -Level Warn -Message "$($currentWeb.Url) does not have the afcent.eventreceiver.newdoclib.wsp feature activated.";
	Write-Log -Level Warn -Message "Activating afcent.eventreceiver.newdoclib.wsp feature activated.  Run Script again to create doc lib";
	$g = new-object System.Guid("bba312fb-1d35-45bf-8cb8-2ac50ecb32ba");
	$currentWeb.Features.Add($g);
	$currentWeb.Update();	
   }
   }
   else  
   {
	Write-Log -Level Warn -Message "$($currentWeb) already contains a list name $($title)";
   }
   $currentWeb.Dispose();
}

# ----------------------------------------------------------
# Create SPN for Central Administration
# ----------------------------------------------------------
function Global:Update-DocumentLibrary
{
   param([Microsoft.SharePoint.PowerShell.SPWebPipeBind]$web,[string]$title)
   $currentWeb = $web.Read();
   $list = $currentWeb.Lists[$title];
   if( $list -ne $null)
   {     
   	 $list.OnQuickLaunch = $true;
   	 $list.Update();
	 Write-Log -Level Info -Message "$($currentWeb) list name $($title) has been updated";
   }
   else  
   {
	Write-Log -Level Warn -Message "$($currentWeb) does not contains a list named $($title)";
   }
   $currentWeb.Dispose();
}


# ----------------------------------------------------------
# Create SPN for Central Administration
# ----------------------------------------------------------
function Global:Remove-DocumentLibrary
{
   param([Microsoft.SharePoint.PowerShell.SPWebPipeBind]$web,[string]$internalName,[string]$title)
   $currentWeb = $web.Read();
   $list = $currentWeb.Lists | ?{ ($_.Title -eq $title) -and ($_.DefaultViewUrl -like "*$($internalName)*") }
   if($list -ne $null)
   {     
	Write-Log -level info -message "Removing document library $($title) on $($currentWeb.Url)";
   	$list = $currentWeb.Lists[$title]; 
   	$list.AllowDeletion = $true;
   	$list.Update();
	$list.Delete();
   	
   }
   else
   {
 	Write-Log -Level Warn -Message "$($currentWeb) does not have a library named $($title)";

   }
   $currentWeb.Dispose();
}

# ----------------------------------------------------------
# Create SPN for Central Administration
# ----------------------------------------------------------
function Global:Update-ThemeOptions
{
	param([Microsoft.SharePoint.PowerShell.SPWebPipeBind]$web)
	$cWeb = $web.Read();
	Write-Log -Level Info -Message "Updating theme gallery at $($cWeb.Url)";
	$titles = $cWeb.Lists["Composed Looks"].Items | ?{ $_.Title -ne "Office" }
	$titles |  %{ $_.Recycle() }
	$dColor = $cWeb.Lists["Theme Gallery"].Items | ?{ $_.Name -like "*spcolor" -and $_.name -like "palette*" -and $_.name -notlike "*005*" -and $_.name -notlike "*032*" -and $_.name -notlike "*001*" -and $_.name -notlike "*003*"} 
	$dColor | %{ $_.Recycle(); }
	$cWeb.Dispose();

}

# ----------------------------------------------------------
# Create SPN for Central Administration
# ----------------------------------------------------------
function Global:Construct-SPGroupName
{
	param([string]$siteUrl)
	$managedPaths = $mps | select -expandproperty path
	$siteUrl = $siteUrl.TrimStart("/");
	
	if($siteUrl.Split('/').Count -gt 2 )
	{
		$match = $managedPaths | ?{$_ -like "*/*"} | ?{ ($siteUrl -like "$($_)*") };
		$newUrl = $siteUrl.Replace("$($match)","");
		$groupName = $newUrl.TrimStart("/").Replace("/","-");
		
	}
	else
	{
		$groupName = $siteUrl.TrimStart("/");
	}
	Write-Log -Level Info -Message "Constructed SP group name $($groupName) from $($siteUrl)";
	return $groupName;
}

# ----------------------------------------------------------
# Create SPN for Central Administration
# ----------------------------------------------------------
function Global:Lookup-Permissions
{
   param([string]$webUrl)
   Write-Log -Level Info -Message "Looking up permissions for $($webUrl)";
   $groups = $permWebs | ?{$_.Url -eq $webUrl } |  %{$_.Member.Login } | sort -unique;
   return $groups;
}

# ----------------------------------------------------------
# Create SPN for Central Administration
# ----------------------------------------------------------
function Global:Apply-DefaultTheme
{
	param([Microsoft.SharePoint.PowerShell.SPWebPipeBind]$web)
	$cWeb = $web.Read();
	$rWeb = $cWeb.Site.RootWeb;
	Write-Log -Level Info -Message "Applying Theme to $($cWeb).";
	$font = "$($rWeb.ServerRelativeUrl)/_catalogs/theme/15/SharePointPersonality.spfont";
	$color = "$($rWeb.ServerRelativeUrl)/_catalogs/theme/15/Palette001.spcolor";
	$img = "/_layouts/15/1033/afcent/images/backgrounds/bg.png"
	$cWeb.ApplyTheme($color,$font,$img,$false);
	$cWeb.Update();
	$rWeb.Dispose();
	$cWeb.Dispose();
}

# ----------------------------------------------------------
# Create SPN for Central Administration
# ----------------------------------------------------------
function Global:Create-SPAudience
{
	param([string]$name,[Microsoft.SharePoint.PowerShell.SPSitePipeBind]$site,$rules)
	$spSite = $site.Read();
	$ctx = Get-SPServiceContext $spSite;
	$audMgr = new-object Microsoft.Office.Server.Audience.AudienceManager($ctx);
	if( $audMgr.Audiences[$name] -eq $null )
	{
		$newAud = $audMgr.Audiences.Create($name, "");
		$newAud.AudienceRules = new-object System.Collections.ArrayList;
		$rules | %{ 
			Write-Log -Level Info -Message "Creating rule for $($name) with $($_.adproperty) $($_.operator) $($_.value) ";
			$rule = new-object Microsoft.Office.Server.Audience.AudienceRuleComponent($_.adproperty,$_.operator,$_.value);
			$newAud.AudienceRules.Add($rule);
		}

		$newAud.Commit();
	}
	else
	{
		Write-Log -Level Warn "Audience '$($name)' already exists.";
	}
	$spSite.Dispose();

}

# ----------------------------------------------------------
# Create SPN for Central Administration
# ----------------------------------------------------------
function Global:Add-LocalAdministrator
{
   param([string]$domain,[string]$userName,[string]$computername)
   Write-Log -Level Info -Message "Adding $($domain)\$($userName) as local administrator on $($computername)";
   ([ADSI]"WinNT://$computerName/Administrators,group").Add("WinNT://$($domain)/$($userName)")  

}

#============================================================================================#
#							Host File Functions Functions		     #
#============================================================================================#

# ----------------------------------------------------------
# Create SPN for Central Administration
# ----------------------------------------------------------
function Global:Add-HostsFileEntry
{
	param([string]$fileName, [string]$ip, [string]$hostName)
	  #first call remove to clear out any old entries for this
	  Remove-HostsFileEntry -fileName $fileName -hostname $hostName;
	  $ip + "`t`t" + $hostName | out-file -encoding ASCII -append $fileName -force;
}

# ----------------------------------------------------------
# Create SPN for Central Administration
# ----------------------------------------------------------
function Global:Remove-HostsFileEntry
{
	param([string]$fileName, [string]$hostName)
		#Get the contents of the current file
		$content = GC $fileName
		$newSection = @();
		$content | %{
				$match = [regex]::split($_, "\t+");
				if( $match.count -eq 2 )
				{
				  if($match[1] -ne $hostName)
				  {
				     $newSection += $_;
				  }
				}#ifmatch eq 2
				else
				{
					$newSection += $_;
				}
		}#foreach line
		#Clear out the old file and add new section
		Clear-Content $fileName;
		$newSection | %{
			$_ | out-file -encoding ASCII -append $fileName -force;
		}
}

# ----------------------------------------------------------
# Create SPN for Central Administration
# ----------------------------------------------------------
function Global:Display-HostsFile
{
	param([string]$fileName)
	$c = GC $filename
	$c |%{
		$match = [regex]::Split($_, "\t+")
		if ($match.count -eq 2) {
			Write-Host $match[0] `t`t $match[1]
		}
	}

}
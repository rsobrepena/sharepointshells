﻿Add-PSSnapin Microsoft.SharePoint.PowerShell
Import-Module D:\3-INSTALL-SITEMOD\SharePointAPI.psm1

write-host "Getting site collection URLs..."
Get-SPSite -WebApplication https://portal.afcent.af.mil -Limit all | Filter-OrgSites | foreach{$_.Url} | Out-File D:\3-INSTALL-SITEMOD\1_Site_Urls.txt

write-host "Getting web URLs..."
Get-SPSite -WebApplication https://portal.afcent.af.mil -Limit all | Filter-OrgSites | Get-SPWeb -Limit all | Filter-OrgWebs | foreach{$_.Url} | Out-File D:\3-INSTALL-SITEMOD\2_Web_Urls.txt

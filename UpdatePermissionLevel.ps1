$sites = get-spsite -Limit ALL –webapplication http://portal.afcent.af.mil           
ForEach ($site in $Sites)            
{             
    # The URL to the Site Collection            
             
    # Enter the Name of the Permission Level to Change            
    # AFCENT WEB ADMINS
    $PermissionLevel=$site.RootWeb.RoleDefinitions["AFCENT Web Admins"]            
    # AFCENT CONTRIBUTE AND SECURE
    $PermissionLevel=$site.RootWeb.RoleDefinitions["AFCENT Contribute and Secure"]            
             
    # Enter all the permissions that Permission Level should have enabled            
    # AFCENT WEB ADMINS    
    $PermissionLevel.BasePermissions="ManageLists, CancelCheckout, AddListItems, EditListItems, DeleteListItems, ViewListItems, OpenItems, ViewVersions, CreateAlerts, ViewFormPages, AddAndCustomizePages, ApplyThemeAndBorder, ApplyStyleSheets, BrowseDirectories, CreateSSCSite, ViewPages, EnumeratePermissions, BrowseUserInfo, ManageAlerts, UseRemoteAPIs, UseClientIntegration, Open, EditMyUserInfo";
    # AFCENT CONTRIBUTE AND SECURE
    $PermissionLevel.BasePermissions="ManageLists, CancelCheckout, AddListItems, EditListItems, DeleteListItems, ViewListItems, ApproveItems, OpenItems, ViewVersions, CreateAlerts, ViewFormPages, ManagePermissions, ViewUsageData, ManageWeb, AddAndCustomizePages, BrowseDirectories, CreateSSCSite, ViewPages, EnumeratePermissions, BrowseUserInfo, ManageAlerts, UseRemoteAPIs, UseClientIntegration, Open, EditMyUserInfo";
     
    write-host "Update Site: " $site.Url;         
    $PermissionLevel.Update()            
    $site.Dispose()            
}
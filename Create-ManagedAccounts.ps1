
#load the global and helper functions
#get the location of the execution of this PS1 file
$path = Split-Path -parent $MyInvocation.MyCommand.Definition
. $path\SharePoint_Helper.ps1
. $path\SharePoint_Globals.ps1  

 Write-Log -Level Info  -Message "Please Enter the Password for the $($crawlaccount)";
    $crawlCred = Get-Credential $crawlaccount
	New-SPManagedAccount -Credential $crawlCred -EA 0
 Write-Log -Level Info  -Message "Please Enter the Password for the $($apppoolaccount)";
    $poolCred = Get-Credential $apppoolaccount
	New-SPManagedAccount -Credential $poolCred -EA 0
if( $network -ne "n")
{
 Write-Log -Level Info  -Message "Please Enter the Password for the $($relapppoolaccount)";
    $poolCred = Get-Credential $relapppoolaccount
	New-SPManagedAccount -Credential $poolCred -EA 0
}
Write-Log -Level Info  -Message "Please Enter the Password for the $($serviceaccount)";    
    $serviceCred = Get-Credential $serviceaccount
	New-SPManagedAccount -Credential $serviceCred -EA 0

if( $network -eq "n")
{
	Write-Log -Level Info  -Message "Please Enter the Password for the $($emsAppPoolName)";    
    $serviceCred = Get-Credential $emsAppPoolName
	New-SPManagedAccount -Credential $serviceCred -EA 0
}

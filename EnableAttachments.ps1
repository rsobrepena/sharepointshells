﻿Add-PSSnapin "Microsoft.sharepoint.powershell"
$webapp = get-spwebapplication https://portal.afcent.af.mil 
$sites = $webapp.sites
foreach($site in $sites) { 
    foreach($web in $site.allwebs) { 
        $lists = $web.lists
        foreach($list in $lists) { 
            if (( $list.ParentWebUrl -ne "/") -and 
            ($list.ParentWebUrl -ne "/help") -and 
            ($list.ParentWebUrl -ne "/mgt") -and 
            ($list.ParentWebUrl -ne "/search") -and 
            ($list.ParentWebUrl -notlike "/c/*") -and 
            ($list.ParentWebUrl -notlike "*/ecoi*") -and 
            ($list.ParentWebUrl -notlike "*/lcoi*") -and 
            ($list.ParentWebUrl -notlike "/personal/*") -and 
            ($list.ParentWebUrl -notlike "/sites/*") -and 
            ($list.GetType().Name -eq "SPList") -and 
            ($list.EnableAttachments -eq $false) -and 
            ($list.Hidden -ne $true) -and 
            ($list.BaseTemplate-ne "Announcements") -and 
            ($list.BaseTemplate -ne "Contacts") -and 
            ($list.BaseTemplate -ne "Links") -and 
            ($list.BaseTemplate -ne "Tasks") -and 
            ($list.BaseTemplate -ne "DocumentLibrary") -and 
            ($list.BaseTemplate -ne "Survey") -and 
            ($list.Title -ne "Workflow Tasks") -and 
            ($list.Title -ne "Content and Structure Reports") -and 
            ($list.Title -ne "Announcements") -and 
            ($list.Title -ne "Contacts") -and 
            ($list.Title -ne "Links") -and 
            ($list.Title -ne "Reusable Content") -and 
            ($list.Title -ne "Tasks") -and 
            ($list.Title -ne "Microfeed") -and 
            ($list.Title -notlike "ewar*") -and 
            ($list.Title -notlike "WP_*")) { 
                $list.enableattachments = $true;
                $list.update();
                $msg = $list.Title + " at " + $list.parentweburl + " has been updated!" | out-file d:\EnabledAttachments.txt -Append
            }
        }
    }
}
write-host "Done!"

Select ParentWebUrl, Title | Out-File D:\EnableAttachments.txt 
        
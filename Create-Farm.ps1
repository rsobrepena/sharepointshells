
#==============================================================
# Microsoft provides programming examples for illustration only, 
# without warranty either expressed or implied, including, but not 
# limited to, the implied warranties of merchantability and/or 
# fitness for a particular purpose. 
# This sample assumes that you are familiar with the programming 
# language being demonstrated and the tools used to create and debug 
# procedures. Microsoft support professionals can help explain the 
# functionality of a particular procedure, but they will not modify 
# these examples to provide added functionality or construct 
# procedures to meet your specific needs. if you have limited 
# programming experience, you may want to contact a Microsoft 
# Certified Partner or the Microsoft fee-based consulting line at 
# (800) 936-5200. 
# for more information about Microsoft Certified Partners, please 
# visit the following Microsoft Web site:
# https://partner.microsoft.com/global/30000104 
#
# Author: Tim Quinlan (tim.quinlan@microsoft.com)
#
# ----------------------------------------------------------
# History
# ----------------------------------------------------------
param([System.Management.Automation.PsCredential]$farmCred)

#load the global and helper functions
#get the location of the execution of this PS1 file
$path = Split-Path -parent $MyInvocation.MyCommand.Definition
. $path\SharePoint_Helper.ps1
. $path\SharePoint_Globals.ps1  

if ( Test-SharePointServer )
{
  New-SPConfigurationDatabase -DatabaseName $configDB -DatabaseServer $databaseServerName `
	-AdministrationContentDatabaseName $adminDB -Passphrase `
         (ConvertTo-SecureString -string $passphrase -AsPlainText -Force) -FarmCredentials $farmCred -SkipRegisterAsDistributedCacheHost;
  Install-SPHelpCollection -All
  Initialize-SPResourceSecurity
  Install-SPService
  Install-SPFeature -AllExistingFeatures
  New-SPCentralAdministration -Port $caPort -WindowsAuthProvider $caAuth
  Install-SPApplicationContent
  
  if(!(test-path $spLogPath))
  {
    new-item $spLogPath -itemtype directory;
  }
  #Set Log file location
  Set-SPDiagnosticConfig -LogLocation $spLogPath -DaysToKeepLogs 4;
 
  #Set Email Settings
  $ca = Get-SPWebApplication -IncludeCentralAdministration | ?{ $_.IsAdministrationWebApplication }
  Set-SPWebApplication -Identity $ca -SMTPServer $smtpServer -ReplyToEmailAddress $smtpReply -OutgoingEmailAddress $smtpFrom;

  #Set Timer Job clean up history
  $tj = Get-SPTimerJob | ?{ $_.Name -like "*job-delete-job*" }
  $tj.DaysToKeepHistory = 1;
  $tj.Update();

  Set-SPTimerJob -Identity $tj -Schedule "Daily at 05:00:00";
   #Set Windows Token Time out to 10 minutes
   Set-SPSecurityTokenServiceConfig -WindowsTokenLifetime 10;
}
else
{
   Write-Log -Level Error -Message "Please ensure that SharePoint Server 2013 is installed";
 

}
﻿Add-PSSnapin "Microsoft.sharepoint.powershell"
get-spsite -webapplication https://portal.afcent.af.mil -limit all | 
select -expandproperty AllWebs | 
    select -ExpandProperty lists | 
        where { $_.ParentWebUrl -ne "/" -and 
        $_.ParentWebUrl -ne "/help" -and 
        $_.ParentWebUrl -ne "/mgt" -and 
        $_.ParentWebUrl -ne "/search" -and 
        $_.ParentWebUrl -notlike "/c/*" -and 
        $_.ParentWebUrl -notlike "*/ecoi*" -and 
        $_.ParentWebUrl -notlike "*/lcoi*" -and 
        $_.ParentWebUrl -notlike "/personal/*" -and 
        $_.ParentWebUrl -notlike "/sites/*" -and 
        $_.GetType().Name -eq "SPList" -and 
        $_.EnableAttachments -eq $false -and 
        $_.Hidden -ne $true -and 
        $_.BaseTemplate-ne "Announcements" -and 
        $_.BaseTemplate -ne "Contacts" -and 
        $_.BaseTemplate -ne "Links" -and 
        $_.BaseTemplate -ne "Tasks" -and 
        #$_.BaseTemplate.tostring() -ne "DocumentLibrary" -and 
        $_.BaseTemplate -ne "DocumentLibrary" -and 
        $_.BaseTemplate -ne "Survey" -and 
        $_.Title -ne "Workflow Tasks" -and 
        $_.Title -ne "Content and Structure Reports" -and 
        $_.Title -ne "Reusable Content" -and 
        $_.Title -ne "Tasks" -and 
        $_.Title -ne "Microfeed" -and 
        $_.Title -notlike "ewar*" -and 
        $_.Title -notlike "WP_*" } | 

Select ParentWebUrl, Title, BaseTemplate, BaseType, TemplateFeatureId | Out-File D:\EnableAttachments.txt 
        
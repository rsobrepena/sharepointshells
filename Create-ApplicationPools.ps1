#load the global and helper functions
#get the location of the execution of this PS1 file
$path = Split-Path -parent $MyInvocation.MyCommand.Definition
. $path\SharePoint_Helper.ps1
. $path\SharePoint_Globals.ps1  
$webApps | %{ 
                if($_.AppPool -ne "" -and $_.AppPool -notlike "*rel*" )
                {
					if( $_.AppPool -like "*ems*" )
					{
						Write-Log -Level Info  -Message "Creating Application Pool $($_.AppPool) with account $($emsAppPoolName)";
						New-SPServiceApplicationPool -Name $_.AppPool -Account (Get-SPManagedAccount $emsAppPoolName)
					}
					else
					{
						Write-Log -Level Info  -Message "Creating Application Pool $($_.AppPool) with account $($apppoolaccount)";
						New-SPServiceApplicationPool -Name $_.AppPool -Account (Get-SPManagedAccount $apppoolaccount)
					}
                
                }
		elseif( $_.AppPool -ne "" )
		{
			Write-Log -Level Info  -Message "Creating REL Application Pool $($_.AppPool)";
                    New-SPServiceApplicationPool -Name $_.AppPool -Account (Get-SPManagedAccount $relapppoolaccount)
 		}
		
                
		
            }
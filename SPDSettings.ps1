﻿Add-PSSnapin microsoft.sharepoint.powershell

# 1. Run this first
# get-spsite -Limit all -WebApplication "https://portal.afcent.af.mil" | select url, allowdesigner, allowrevertfromtemplate, allowmasterpageediting, showurlstructure | Out-File D:\SPDSettingsChangedPre.txt

# 2. Run script block 
#########################################################################
$SPWebApp = Get-SPWebApplication https://portal.afcent.af.mil

$outfile = "d:\SPDSettingsChanged.txt"
$line = ""
$count = 0

foreach ($SPSite in $SPWebApp.Sites)
{
	if ($SPSite -ne $null)
	{	
		#set SPD settings here
		write-host "Checking $SPSite"
        $count++
		
            $SPSite.AllowDesigner = $false
            $SPSite.AllowRevertFromTemplate = $false
            $SPSite.AllowMasterPageEditing = $false
            $SPSite.ShowURLStructure = $false
	}
}
write-host "Complete.  Sites checked: " $count
#########################################################################

# 3. Run this last to confirm settings changed
# get-spsite -Limit all -WebApplication "https://portal.afcent.af.mil" | select url, allowdesigner, allowrevertfromtemplate, allowmasterpageediting, showurlstructure | Out-File D:\SPDSettingsChangedPost.txt

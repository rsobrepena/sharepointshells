

# ==============================================================================================
# 
# # NAME: Fix-IISConfiguration
# 
# AUTHOR: Blair Bigger , Microsoft
# DATE  : 6/29/2009
# 
# COMMENT: This script uses an XML configuration file to update IIS settings per each server
# 
# Modified on 11/27/2012 by Tim Quinlan for SharePoint 2010
# ==============================================================================================
#load the global and helper functions
#get the location of the execution of this PS1 file
$path = Split-Path -parent $MyInvocation.MyCommand.Definition
. $path\SharePoint_Helper.ps1
. $path\SharePoint_Globals.ps1 

	foreach	($s in $cfg.Farm.Servers.ContentServers.ContentServer)	
	{	
		$fullServer = $s.name
		  Write-Log -Level Info  -Message "==================  Working on $($fullServer) ==============" ;
			$ips = @()
			$subnets = @()	
		foreach ($website in $s.WebSites.WebSite)
		{			
			$wip = $website.ip;
			Write-Log -Level Info -Message "Found $($wip)";
			if(($ips -contains($wip)) -eq $false)
			{
				$ips += $wip
				# defaulting to 255.255.255.128 
				$subnets +="255.255.255.128"
			}
		}			
		# For debug only - uncomment the two lines below for troubleshooting
		#$ips
		#$subnets				
		# Add IP addresses to server
		Add-IPAddress -ips $ips -subnets $subnets -serverN $fullServer			
		#Set IP address to Web sites
		foreach ($website in $s.WebSites.Website)
		{	
			$webIP = $website.ip;
			$siteurl = $website.url;
			$file = "\\$($fullServer)\$($hostsFilePath.Replace(":","`$"))";
			#Add a Hosts file entry for each
			if( $siteurl -ne "" )
			{
				Add-HostsFileEntry -fileName $file -ip $webIP -hostname $siteurl;
			}
			if ($website.ssl -eq "yes")
			{										
				$siteurl = $website.url;
				Write-Log -Level Info -Message "Configuring $($siteurl )";
				Write-Log -Level Info -Message "Adding IP Binding to $($siteurl)";
				Add-IPBinding -ip $webIP -ssl $true -webName $siteurl -serverN $fullServer;
				Write-Log -Level Info -Message "Adding SSL Binding to $($siteurl)";
				Add-SSLBinding -ip $webIP -certName $website.pfxfile -serverN $fullServer;
			}
			else
			{
				if (!($website.url.Contains("rep.")))
				{					
					Write-Log -Level Info -Message "Skipping configuration for $($website.url) with $($webIP)"
				}
				else
				{
					$siteurl = $website.url;
					#Write-Log -Level Info -Message $siteurl;
							
					Write-Log -Level Info -Message "Configuring $($siteurl)";
					Write-Log -Level Info -Message "Adding IP Binding to $($siteurl)";
					Add-IPBinding -ip $webIP -ssl $false -webName $siteurl -serverN $fullServer;			
				}
			}
				
		} 

     
     }# end foreach site on the server

﻿param([switch]$permissionOnly=$false)
#load the global and helper functions
#get the location of the execution of this PS1 file
$path = Split-Path -parent $MyInvocation.MyCommand.Definition
. $path\SharePoint_Helper.ps1
. $path\SharePoint_Globals.ps1



#=========================================================
#  Apply Web Application Policy Permissions
#=========================================================

$rep = Read-Host "Do you want to apply the web application policy permissions?(y/n)"; 

if($rep -eq "y" )
{
$webApps | ?{ $_.IsExtended -eq $false } | %{ $wa = $_.Name; $webAppPermissions| %{
	Add-WebApplicationPolicy -userName (New-SPClaimsPrincipal -identity $_.Name -IdentityType WindowsSecurityGroupName).ToEncodedString() -displayName $_.Name -role $_.Permission -WebApplication $wa;
	}
  }
  Verify
}




#=========================================================
#   Add Custom Permission Levels
#=========================================================

$rep = Read-Host "Do you want to add the custom permission levels?(y/n)";
if($rep -eq "y" )
{
# Site Owner Level
			 
	$pName = "AFCENT Web Admins";
	$pDesc = "Custom Permission Level that allows the user to Manage Lists, create pages...";
	Add-PermissionLevel -name $pName -mask $ownerMask -description $pDesc;
	Verify;
# AFCENT Contributor
	
	$pName = "AFCENT Contribute and Secure";
	$pDesc = "Custom Permission Level that allows the user to add/edit/update/delete documents and manage permissions on those documents.";
	Add-PermissionLevel -name $pName -mask $contribMask -description $pDesc;
	Verify;
}


#=========================================================
#  Apply All Other Permissions
#=========================================================
$rep = Read-Host "Do you want to add the permissions to all webs?(y/n)";
if($rep -eq "y" )
{
	Write-Log -Level Info -Message "Begin - Adding Permissions to all Webs";

	
	Get-SPSite -Limit All | Get-SPWeb -Limit All | ?{ ($_.Url -notlike "*personal*") -and ($_.Url -notlike "*search*") -and ($_.Url -notlike "*/my*") -and ($_.Url -notlike "*ems*") -and ($_.Url -notlike "*/a/*") -and ($_.Url -notlike "*/c/*") -and ($_.Url -notlike "*coi/*") } | %{
	#Create Site Groups - Site Level
	if($_.ServerRelativeUrl -eq "/")
	{ 
              $siteName = "PortalHome";
	}
	else
	{
		$siteName = Construct-SPGroupName -siteUrl $_.ServerRelativeUrl;
	}
	$rw = $_.Site.RootWeb;

		  $gc = "USAF-$($siteName)-Contributors";
	
		  $go = "USAF-$($siteName)-Admins";
	
		  $gr = "USAF-$($siteName)-Readers";
	if( !$permissionOnly)
	{
		Add-SiteGroups -web $rw -groupName $gr;
		Add-SiteGroups -web $rw -groupName $gc;
		Add-SiteGroups -web $rw -groupName $go;
	}
	#Add Users to Site Groups - Site Level
		
		#Add Contributors to Contributors Group
		# Get Group Name - Lookup function
		$adGroups= Lookup-Permissions -webUrl $_.ServerRelativeUrl;
		
		if(($adGroups -ne $null) -and ($adGroups -ne ""))
		{
			$adGroups | %{
			if( $_ -like "*_co")
			{
				Add-UsersToSiteGroups -web $rw -userlogin $_ -groupName $gc -isGroup
			}
			elseif ( $_ -like "*_wa" )
			{
				Add-UsersToSiteGroups -web $rw -userlogin $_ -groupName $go -isGroup
			}
			elseif ( $_ -like "*_ow" )
			{
				Add-UsersToSiteGroups -web $rw -userlogin $_ -groupName $gc -isGroup
			}
			}
		}

		#Add Authenticated Users to Reader Group
		Add-UsersToSiteGroups -web $rw -userlogin "NT Authority\Authenticated Users" -groupName $gr -isGroup

		
		
	    $_.Update();
		if( !$permissionOnly)
		{
			if( $_.Url -ne $rw.Url  )
			{
				Write-Log -Level Info -Message "Breaking permission inheritance on $($_.Url)";
				$_.BreakRoleInheritance($false);
				$_.Update();
				
			}
		}
		#Add CAC Anonymous if NIPR  Users to Reader Group
		if( $network -eq "n")
		{
			Add-UsersToSiteGroups -web $rw -userlogin "$($domain)-$($network)\$($domain).certmap.svc" -groupName $gr
		}
		else
		{
		   Add-AnonymousAccessToWeb -web $_;
		}
		#Add Permission to Site Groups -Web level
		#Break Permission Inheritance
		if( !$permissionOnly)
		{ 
			#Give Owner group AFCENT Site Owner rights
			Add-PermissionToWeb -web $_ -groupName $go -mask "AFCENT Web Admins"
			#Give Contributors AFCENT_Contributor rights
			Add-PermissionToWeb -web $_ -groupName $gc -mask "Contribute"
			#Give Authenticated Users Read rights
			Add-PermissionToWeb -web $_ -groupName $gr -mask "Read"
		}
	$rw.Dispose();
	$_.Dispose();
	} #Site Loop
	
	#archive
	Write-Log -Level Info -Message "Begin - Adding Permissions to archive all Webs";
	Get-SPSite -Limit All | Get-SPWeb -Limit All | ?{ $_.Url -like "*/a/*" } | %{
	   $rw = $_.Site.RootWeb;
	   $gv = $rw.SiteGroups | ?{ $_.Name -like "*Archive Visitors" };
	   Add-UsersToSiteGroups -web $rw -userlogin "NT Authority\Authenticated Users" -groupName $gv -isGroup
	   if( $network -ne "n")
	   {
	      Add-AnonymousAccessToWeb -web $_;
	   }
	} #archive loop

}
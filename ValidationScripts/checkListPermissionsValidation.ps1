param(

[string] $siteUrl,
[string] $fileLocation,
[string] $fileName

)

function GetSubSites($subWeb){
    
    foreach($list in $subWeb.Lists){
        $row = $table.NewRow()
        $row.Title = $list.Title
        $row.BaseType = $list.BaseType
        $row.DefaultViewUrl = $list.DefaultViewUrl
        $row.ParentWebUrl = $list.ParentWebUrl
        $row.HasUniqueRoleAssignments = $list.HasUniqueRoleAssignments

        $title = $list.Title
        
        if($title -eq "Udeid Documents" -or $title -eq "Documents" -or $title -eq "Pages" -or $title.ToLower().StartsWith("wp_")){


            if($list.HasUniqueRoleAssignments -eq $false){
                $row.ComplianceViolation = $true
            }else{
            
                $row.ComplianceViolation = $false
            }
        }
        
        $table.Rows.Add($row)

        if($title -eq "Pages"){
                $spQuery = New-Object Microsoft.SharePoint.SPQuery
                $camlQuery = "<Where><Eq><FieldRef Name ='Title'/><Value Type='Text'>default</Value></Eq></Where>"
                $spQuery.Query = $camlQuery
                $items = $list.GetItems($spQuery)

                #$item = New-Object Microsoft.SharePoint.SPListItem
                
                foreach($item in $items){
                    $row = $table.NewRow()
                    $row.DefaultAspxWeb = $item.Web
                    $row.DefaultAspxUrl = $item.Url

                    if($item.HasUniqueRoleAssignments -eq $false){
                        $row.ComplianceViolation = $true
                    }else{
                        $row.ComplianceViolation = $false
                    }

                    $table.Rows.Add($row)
                }
        }
          
    }
    if($subWeb.Webs.Count -gt 0){

        foreach($tempSubWeb in $subWeb.Webs){
            GetSubSites($tempSubWeb)
        }        
    }
}

Add-PSSnapin Microsoft.SharePoint.PowerShell

$table = New-Object System.Data.DataTable "ListPermissionsCheck"

$colTitle = New-Object System.Data.DataColumn Title,([string])
$colBaseType = New-Object System.Data.DataColumn BaseType,([string])
$colDefaultViewUrl = New-Object System.Data.DataColumn DefaultViewUrl,([string])
$colParentWebUrl = New-Object System.Data.DataColumn ParentWebUrl,([string])
$colUnique = New-Object System.Data.DataColumn HasUniqueRoleAssignments,([boolean])
$colDefaultAspx = New-Object System.Data.DataColumn DefaultAspxWeb,([string])
$colDefaultUrl = New-Object System.Data.DataColumn DefaultAspxUrl,([string])
$colCompliance = New-Object System.Data.DataColumn ComplianceViolation,([boolean])

$table.Columns.Add($colTitle)
$table.Columns.Add($colBaseType)
$table.Columns.Add($colDefaultViewUrl)
$table.Columns.Add($colParentWebUrl)
$table.Columns.Add($colUnique)
$table.Columns.Add($colDefaultAspx)
$table.Columns.Add($colDefaultUrl)
$table.Columns.Add($colCompliance)

$date = Get-Date

if(!$fileName){
    $fileName = "ListPermissionsReport-" + $date.Month + "." + $date.Day + "." + $date.Year + ".csv"
}else{
    if(!$fileName.EndsWith(".csv")){
        $fileName = $fileName + ".csv"
    }

}

if(!$fileLocation.EndsWith("\")){
    $fileLocation = $fileLocation + "\"
}

$fileFullPath = $fileLocation + $fileName

$rootSite = Get-SPSite $siteUrl
$webapp = $rootSite.WebApplication

foreach ($webappSite in $webapp.Sites){
       
       $web = $webappSite.OpenWeb()
       GetSubSites($web)
       
}

$table | Export-Csv $fileFullPath
$rootSite.Dispose()
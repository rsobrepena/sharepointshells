﻿#load the global and helper functions
#get the location of the execution of this PS1 file
$path = Split-Path -parent $MyInvocation.MyCommand.Definition
. $path\SharePoint_Helper.ps1
. $path\SharePoint_Globals.ps1  

#Determine if we are on SharePoint 2013 server or OWA Server
if( Test-SharePointServer )
{
  #run configuration for SharePoint Server
  New-SPWOPIBinding -ServerName $wacServerName
  Set-SPWOPIZone -zone "external-https";


}
else
{
   #Run configuration for OWA server
   New-OfficeWebAppsFarm -InternalUrl $intWACUrl -ExternalUrl $extWACUrl -CertificateName $wacCertName -CacheLoaction $wacCacheLoaction -EditingEnabled -RenderingLocalCacheLocation $wacRenderLocation

}

#==============================================================
# Microsoft provides programming examples for illustration only, 
# without warranty either expressed or implied, including, but not 
# limited to, the implied warranties of merchantability and/or 
# fitness for a particular purpose. 
# This sample assumes that you are familiar with the programming 
# language being demonstrated and the tools used to create and debug 
# procedures. Microsoft support professionals can help explain the 
# functionality of a particular procedure, but they will not modify 
# these examples to provide added functionality or construct 
# procedures to meet your specific needs. if you have limited 
# programming experience, you may want to contact a Microsoft 
# Certified Partner or the Microsoft fee-based consulting line at 
# (800) 936-5200. 
# for more information about Microsoft Certified Partners, please 
# visit the following Microsoft Web site:
# https://partner.microsoft.com/global/30000104 
#
# Author: Tim Quinlan (tim.quinlan@microsoft.com)
#
# ----------------------------------------------------------
# History
# ----------------------------------------------------------


function SetFormValidationSettings()
{
  $timespan = new-object System.TimeSpan(0,30,0);
  Get-SPWebApplication | ?{ ( $_.FormDigestSettings.Timeout.CompareTo($timespan) -gt 0 ) -or 
	( $_.FormDigestSettings.Enabled -eq $false ) } | %{ $_.FormDigestSettings.Timeout = $timespan; 
          $_.FormDigestSettings.Enabled = $true; $_.Update(); }
  

}


function SetAutoSiteDeleteToFalse() # SV-38109r1_rule
{
   Get-SPWebApplication | ?{  $_.AutomaticallyDeleteUnusedSiteCollections -eq $true } | %{  
          $_.AutomaticallyDeleteUnusedSiteCollections= $false; $_.Update(); }

}

function SetGroupEditRightsToOwner() # SV-38147r1_rule
{
 Get-SPWebApplication | Get-SPSite -Limit All | Get-SPWeb -Limit 1| 
  %{ $_.SiteGroups | ?{ $_.AllowMembersEditMembership -eq $true } | %{ $_.AllowMembersEditMembership = $false; $_.Update(); } }

}

function SetGroupViewRightsToMember() # SV-38152r1_rule
{
 Get-SPWebApplication | Get-SPSite -Limit All | Get-SPWeb -Limit 1| 
  %{ $_.SiteGroups | ?{ $_.OnlyAllowMembersViewMembership -eq $false } | %{ $_.AllowMembersEditMembershipOnly = $true; $_.Update(); } }

}

function SetPasswordPolicyMinAttempts() #SV-37957r1_rule
{

  if( (Get-SPFarm).PasswordChangeMaximumTries -gt 3 )
  { 
	(Get-SPFarm).PasswordChangeMaximumTries = 3;
        (Get-SPFarm).Update();
  }
}

function SetPolicyForAuditing() #SV-37767r1_rule
{
    $pList = [Microsoft.Office.RecordsManagement.InformationPolicy.PolicyCatalog]::FeatureList
    ($pList["Microsoft.Office.RecordsManagement.PolicyFeatures.PolicyAudit"]).State = [Microsoft.Office.RecordsManagement.InformationPolicy.PolicyFeatureState]::Visible;
    ($pList["Microsoft.Office.RecordsManagement.PolicyFeatures.PolicyAudit"]).Update();
} 

function SetUsageLoggingDirPerms() #SV-36596r1_rule
{
  $logdir = ([Microsoft.SharePoint.Administration.SPusageManager]::Local).UsageLogDir;
  $aclList = (Get-Item $logdir).GetACcessControl("Access");
  $acl
    

}

function DisableAccessToOnlineWPGallery() #SV-37994r1_rule
{
  Get-SPWebApplication | ?{ $_.AllowAccessToWebPartCatalog -eq $true } | %{ $_.AllowAccessToWebPartCatalog = $false; $_.Update(); }

}

function EnabledUsageDataCollection() #SV-36713r1_rule
{
    if (!([Microsoft.SharePoint.Administration.SPusageManager]::Local).UsageLogDiskUsageEnabled)
    {
	([Microsoft.SharePoint.Administration.SPusageManager]::Local).UsageLogDiskUsageEnabled = $true
	([Microsoft.SharePoint.Administration.SPusageManager]::Local).Update();
    }
}
#load the global and helper functions
#get the location of the execution of this PS1 file
$path = Split-Path -parent $MyInvocation.MyCommand.Definition
. $path\SharePoint_Helper.ps1
. $path\SharePoint_Globals.ps1
$buildSIPR=$false;
$webApps | ?{ ($_.ParentApp -eq $null) -and ($_.Name -notlike "*ems*")} | %{ $wa = $_.Name; $mps | %{ 
  if( $network -eq "s" )
  { 
     $buildSIPR=$true;

  }
  if( $buildSIPR )
  {
   	if( $_.Explicit -eq $true )
   	{
		Write-Log -Level Info  -Message "Building explicit Managed Path $($_.Path) on Web Application $($wa)" ;
		New-SPManagedPath -RelativeUrl $_.Path -WebApplication $wa -Explicit 
		Start-Sleep 1;
   	}
   	else{
		Write-Log -Level Info  -Message "Building wildcard Managed Path $($_.Path) on Web Application $($wa)" ; 
		New-SPManagedPath -RelativeUrl $_.Path -WebApplication $wa  
		Start-Sleep 1;
             }
  }
  else
  {
    if( !$_.SIPR )
    {
      if( $_.Explicit -eq $true )
   	{
		Write-Log -Level Info  -Message "Building explicit Managed Path $($_.Path) on Web Application $($wa)" ;
		New-SPManagedPath -RelativeUrl $_.Path -WebApplication $wa -Explicit 
		Start-Sleep 1;
   	}
   	else{
		Write-Log -Level Info  -Message "Building wildcard Managed Path $($_.Path) on Web Application $($wa)" ; 
		New-SPManagedPath -RelativeUrl $_.Path -WebApplication $wa  
		Start-Sleep 1;
             }
    }
  }
 }
}

		
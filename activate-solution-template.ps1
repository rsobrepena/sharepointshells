# Get a reference to the target site

$web = Get-SPWeb http://portal.afcent.af.mil/A6/CertificationTracker

# Update the property bage value and set it  to the string value "true"

$web.AllProperties["SaveSiteAsTemplateEnabled"] = "true"

# Commit the property change to server

$web.Update()

 
$stsadm = 'C:\Program Files\Common Files\Microsoft Shared\web server extensions\12\BIN\stsadm.exe'
function get-webpage([string]$url,[System.Net.NetworkCredential]$cred=$null)  
 {  
   $wc = new-object net.webclient  
   if($cred -eq $null)  
   {  
     $cred = [System.Net.CredentialCache]::DefaultCredentials;  
   }  
     $wc.credentials = $cred;  
     return $wc.DownloadString($url);  
}  


 $cred = [System.Net.CredentialCache]::DefaultCredentials;  
 $sc = Get-SPSite -limit all | select Url 
     $sc |  %{  
         write-host "Connecting to $($_.Url)";  
         $html=get-webpage -url $_.Url -cred $cred;  
     }  
 


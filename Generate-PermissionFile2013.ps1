﻿Add-PSSnapin microsoft.sharepoint.powershell

function WriteXml($str)
{
    $str | out-file d:\build\powershellscripts\AFCENT_Permissions2013.xml -Append
}

function EnumPerms($web)
{
    Write-Host $web.url
    $gText="";

    foreach ($role in $web.RoleAssignments)
    {
        if ($role.Member.Name -like "*Admin*")
        {
            #Write-Host "role = " $role.Member.Name
            foreach ($group in $web.Groups)
            {
                #Write-Host "group = " $group.Name
                foreach ($user in $group.Users)
                {
                    if ($user.DisplayName -like "*_ow*" -or $user.DisplayName -like "*_wa*")
                    {
                        #Write-Host "<Member Login='$($user.DisplayName)' />`n";
                        $gText += "<Member Login='$($user.DisplayName)' />`n";
                    }
                }
            }
        }
    }

    if ($gText -ne "")
    {
        $wUrl = $web.ServerRelativeUrl.Replace("/shared", "").Replace("/public","").Replace("/private","").Replace("/l/","/");
        #Write-Host $gText
        WriteXml("<Web Url='$($wUrl)'>");
        WriteXml($gText);
        WriteXml("</Web>");
    }

    $gText="";
    $web.Dispose();
}
#endfunction

WriteXml("<?xml version='1.0'?>");
WriteXml("<webs>");

#AFCENT
$webapp = Get-SPSite -webapplication "https://portal.afcent.af.mil" -Limit ALL

foreach ($site in $webapp)
{
    foreach ($web in $site.AllWebs)
    {
        if (($web.Url -notlike "*/a/*") -and ($web.Url -notlike "*/personal/*") -and ($web.Url -notlike "*sites/my*") -and ($web.Url -notlike "*/ecoi*") -and ($web.Url -notlike "*/lcoi*") -and ($web.Url -notlike "*/coi*") -and ($web.Url -notlike "*/c/*"))
        {
            EnumPerms($web);
        }

    }
}

WriteXml("</webs>");

Write-Host "Complete - Please copy AFCENT_Permissions.xml to the SharePoint 2013 D:\Build\PowerShell scripts folder";

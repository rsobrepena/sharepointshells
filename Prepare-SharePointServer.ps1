param([switch]$full,[switch]$createFarm,[switch]$createDBs,[switch]$addservers,[switch]$addaccounts,[switch]$addPermissions,[switch]$createAppPools,[switch]$createWebApplications,[switch]$setRegistry,[switch]$setIIS,[switch]$CreateStructure,[switch]$SetSSL,[switch]$createServices)
#load the global and helper functions
#get the location of the execution of this PS1 file
$path = Split-Path -parent $MyInvocation.MyCommand.Definition
. $path\SharePoint_Helper.ps1
. $path\SharePoint_Globals.ps1  

#Clear any errors
$error.Clear();

if( Test-Path "SharePoint_Install_Log.log")
{
  Remove-Item "SharePoint_Install_Log.log";
}
if ( Test-SharePointServer )
{
   Write-Log -Level Info  -Message "Please enter the $($farmaccount) username and password" ;
   $cred = Get-Credential $farmaccount
   if($cred -eq $null)
   {
	exit;
   }
#=====================================
#    Update Registry
#=====================================
 if( $full -or $setRegistry)
   {
       $answer = Read-Host "Do you want to set the registry values?(y/n)";
	   if( $answer -eq "y" )
	   {
		  Write-Log -Level Info  -Message "Setting Registry Keys on $($_.Name)";
	      $servers | %{

		  #Kerberos on non-Standard Ports
		  New-RegistryKey -computer $_.Name -keyPath "HKLM:\SOFTWARE\WOW6432Node\Microsoft\Internet Explorer\Main\FeatureControl" -keyName "FEATURE_INCLUDE_PORT_IN_SPN_KB908209";
		  Set-RegistryKey -computer $_.Name -keyPath "HKLM:\SOFTWARE\WOW6432Node\Microsoft\Internet Explorer\Main\FeatureControl\FEATURE_INCLUDE_PORT_IN_SPN_KB908209" -keyName "iexplore.exe" -keyValue 1 -keyType "DWord"

		  #Fix for Start Up issue 
		  Set-RegistryKey -computer $_.Name -keyPath "HKLM:\System\CurrentControlSet\Services\HTTP" -keyName "DependonService" -keyValue 'CRYPTSVC' -keyType "MultiString";

		  #LoopbackCheck
		  Set-RegistryKey -computer $_.Name -keyPath HKLM:\SYSTEM\CurrentControlSet\Control\Lsa -keyName "DisableLoopbackCheck" -keyValue 1 -keyType "DWord"

		  #Chimney Offload
		  Set-RegistryKey -computer $_.Name -keyPath HKLM:\SYSTEM\CurrentControlSet\Services\Tcpip\Parameters -keyName "EnableTCPA" -keyValue 0 -keyType "DWord"
		  Set-RegistryKey -computer $_.Name -keyPath HKLM:\SYSTEM\CurrentControlSet\Services\Tcpip\Parameters -keyName "EnableRSS" -keyValue 0 -keyType "DWord"
		  Set-RegistryKey -computer $_.Name -keyPath HKLM:\SYSTEM\CurrentControlSet\Services\Tcpip\Parameters -keyName "EnableTCPChimney" -keyValue 0 -keyType "DWord"
		  Set-RegistryKey -computer $_.Name -keyPath HKLM:\SYSTEM\CurrentControlSet\Services\Tcpip\Parameters -keyName "DisableTaskOffload" -keyValue 1 -keyType "DWord"
		  Set-RegistryKey -computer $_.Name -keyPath HKLM:\SYSTEM\CurrentControlSet\Services\Tcpip\Parameters -keyName "SynAttackProtect" -keyValue 1 -keyType "DWord"
		  Set-RegistryKey -computer $_.Name -keyPath HKLM:\SYSTEM\CurrentControlSet\Services\Tcpip\Parameters -keyName "EnableDeadGWDetect" -keyValue 0 -keyType "DWord"
		  Set-RegistryKey -computer $_.Name -keyPath HKLM:\SYSTEM\CurrentControlSet\Services\Tcpip\Parameters -keyName "KeepAliveTime" -keyValue 000493e0 -keyType "DWord"
		  Set-RegistryKey -computer $_.Name -keyPath HKLM:\SYSTEM\CurrentControlSet\Services\Tcpip\Parameters -keyName "DisableIPSourceRouting" -keyValue 2 -keyType "DWord"
		  Set-RegistryKey -computer $_.Name -keyPath HKLM:\SYSTEM\CurrentControlSet\Services\Tcpip\Parameters -keyName "TcpMaxConnectResponseRetransmissions" -keyValue 2 -keyType "DWord"
		  Set-RegistryKey -computer $_.Name -keyPath HKLM:\SYSTEM\CurrentControlSet\Services\Tcpip\Parameters -keyName "TcpMaxDataRetransmissions" -keyValue 3 -keyType "DWord"
		  Set-RegistryKey -computer $_.Name -keyPath HKLM:\SYSTEM\CurrentControlSet\Services\Tcpip\Parameters -keyName "PerformRouterDiscovery" -keyValue 0 -keyType "DWord"
		  Set-RegistryKey -computer $_.Name -keyPath HKLM:\SYSTEM\CurrentControlSet\Services\Tcpip\Parameters -keyName "TcpMaxPortsExhausted" -keyValue 5 -keyType "DWord"
		  
		 
		  }
		  Verify
	   }
   }
#=====================================
#    Set Up IIS
#=====================================
   if( $full -or $setIIS)
   {
    $answer = Read-Host "Do you want to configure IIS ?(y/n)";
	if( $answer -eq "y" )
	{
   	#Set IIS Settings
   	$servers | %{ 
			Write-Log -Level Info  -Message "Setting IIS Settings on $($_.name)";
			#ConfigureRemoting -computerName $_.name -client;
        	Set-CustomIISSettings -server $_.name -newDrive "D:" -credentials $cred;
        	
        	}
	Verify
	}
   }

#=====================================
# Create All Content and Non Service application databases
#=====================================  
if( $full -or  $createDBs )
{
   $answer = Read-Host "Do you want to create the configuration and central administration databases?(y/n)";
	if( $answer -eq "y" )
	{
    #Create the Central Admin and ConfigDB
	.\Precreate-AFCENTDatabases.ps1 -config
	Verify
	}
	 $answer = Read-Host "Do you want to create the content databases?(y/n)";
	if( $answer -eq "y" )
	{
	#create all content DBs 
	Write-Log -Level Info  -Message "Precreating all content databases.  This will take a while.";
	.\Precreate-AFCENTDatabases.ps1 -content -archive
	Verify
	}
}

#=====================================
# Create Farm and Add Current Server to farm
#=====================================  

   if( $full -or $createFarm)
   {
    $answer = Read-Host "Do you want to create the SharePoint farm?(y/n)";
	if( $answer -eq "y" )
	{
	Write-Log -Level Info  -Message "Creating the farm and adding current server to the farm.  This will take a few moments";
	.\Create-Farm.ps1 -farmCred $cred;
   	Verify
	}
   }
#=====================================
# Add Additional Servers to the farm
#=====================================
   if( $full -or $addServers)
   {
    $answer = Read-Host "Do you want to add additional servers to the SharePoint farm?(y/n)";
	if( $answer -eq "y" )
	{
     	Write-Log -Level Info  -Message "Adding additional servers into the SharePoint farm" ;
    	try
    	{
   		$servers | 
    		%{
        		if($_.CA -eq $false) #do not try to add the CA Server
        		{
            		   .\Join-ServerToFarm.ps1 -farmCred $cred -computerName $_.name
        		}
				#add Local Administrators
				try
				{
					Add-LocalAdministrator -domain "CENTAF$($forestSuffix)$($network)" -userName "Enterprise Web Administrators" -computername $_.name;
				}
				catch{}
    		}
    	}
    	catch{
     
     	}
    	Verify
	 }
	    $cacheServer = $servers | ?{ $_.Name -notlike "*app*" } | select -First 1 | select -ExpandProperty Name
	    $sb = { Add-PSSnapin Microsoft.SharePoint.PowerShell;
				Use-CacheCluster; Update-SPDistributedCacheSize -CacheSizeInMB 7000; };
	    $remoteSession = New-PSSession -ComputerName $cacheServer -Credential $cred -Authentication Credssp -Verbose
        Invoke-Command -Session $remoteSession -ScriptBlock $sb;
        Remove-PSSession $remoteSession
	 
   }

#=====================================
#  Add Additional Managed Accounts
#=====================================
   if( $full -or $addAccounts)
   {
    $answer = Read-Host "Do you want to add the managed service accounts ?(y/n)";
	if( $answer -eq "y" )
	{
      Write-Log -Level Info  -Message "Adding additional managed accounts into the SharePoint farm" ;
      .\Create-ManagedAccounts.ps1
      Verify 
	}
   }

#=====================================
#  Create Application Pools
#=====================================
  if( $full -or $createAppPools)
  {
    $answer = Read-Host "Do you want to create the application pools?(y/n)";
	if( $answer -eq "y" )
	{
    .\Create-ApplicationPools.ps1
    Verify
	}
  }
#=====================================
#  Create Web Applications
#=====================================
  if( $full -or $createWebApplications)
  {
     $answer = Read-Host "Do you want to create the web applications?(y/n)";
	if( $answer -eq "y" )
	{
     .\Create-WebApplication.ps1
     Verify
	}
  }

#=====================================
#  Configure SSL and IP for Web applications
#=====================================
if( $full -or  $SetSSL)
{
  $rep = Read-Host "Do you want to install SSL certificates on the server? (y/n) ";
  if ( $rep -eq "y")
  {
	  Write-Log -Level Info  -Message "Adding SSL Certificates to farm" ;
	  #scan directory for PFX files
	  $certNames = gci -Path "SSLCertificates" -filter "*.pfx" -Recurse | ?{ $_.FullName -like "*\portal\*" -or $_.FullName -like "*$($farmprefix)*" } | select -ExpandProperty FullName
	  $servers | %{ $sn = $_.Name;
		Write-Log -Level Info  -Message "Adding Certificates to $($sn)";
		$certNames | %{
			Write-Log -Level Info  -Message "Installing certificate $($_)..";
			Install-SSLCertificates -certPath $_ -sslPassword $sslKey -server $sn -cred $cred;    
		
					  }
			Verify
	  }
  }			
   $answer = Read-Host "Do you want to assign IP Addresses and SSL Certificates ?(y/n)";
	if( $answer -eq "y" )
	{
  .\Set-IISConfiguration.ps1;
  Verify
	}
}
#=====================================
#  Add SharePoint Solutions
#=====================================
  #TBD

#=====================================
#  Create Managed Paths
#=====================================
  if( $full -or $CreateManagedPaths)
  {
    $answer = Read-Host "Do you want to create the managed paths?(y/n)";
	if( $answer -eq "y" )
	{
   .\Create-ManagedPaths.ps1
   Verify;
   }
  }

#=====================================
#  Create Site Structure
#=====================================
  if( $full -or $CreateStructure)
  {
    $answer = Read-Host "Do you want to create the site structure?(y/n)";
	if( $answer -eq "y" )
	{
   .\Create-SiteStructure.ps1 -full
   Verify;
   }
  }
#=====================================
# Permission 
#=====================================
  if( $full -or $addPermissions )
  {
     $answer = Read-Host "Do you want to apply permissions?(y/n)";
	if( $answer -eq "y" )
	{
    .\Apply-Permissions.ps1
	Verify
	}
  }
#=====================================
# Provision Services
#=====================================
  if( $full -or $createServices)
  {
     $answer = Read-Host "Do you want to deploye the service applications ?(y/n)";
	if( $answer -eq "y" )
	{
		if( $network -eq "n")
		{
			.\Provision-Services.ps1 -usageServices -accessServices -excelServices -stateServices  -wordViewingServices -powerPTService -metadataServices -searchService2013 -profileService;
		}
		else
		{
			.\Provision-Services.ps1 -usageServices -accessServices -excelServices -stateServices  -wordViewingServices -powerPTService -metadataServices -searchService2013 -profileService -metadataServicesUS;
		}
		Verify;
	}
  }
}#test-SharePointServer

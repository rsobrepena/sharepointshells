function Export-All-SPWeb-Lists([string]$WebURL, [string]$ExportRootPath)
{
	[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.SharePoint") > $null
	[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.SharePoint.Deployment") > $null

	$versions = [Microsoft.SharePoint.Deployment.SPIncludeVersions]::All

	$exportObject = New-Object Microsoft.SharePoint.Deployment.SPExportObject
	$exportObject.Type = [Microsoft.SharePoint.Deployment.SPDeploymentObjectType]::List 
	$exportObject.IncludeDescendants = [Microsoft.SharePoint.Deployment.SPIncludeDescendants]::All

	$settings = New-Object Microsoft.SharePoint.Deployment.SPExportSettings

	$settings.ExportMethod = [Microsoft.SharePoint.Deployment.SPExportMethodType]::ExportAll
	$settings.IncludeVersions = $versions
	$settings.IncludeSecurity = [Microsoft.SharePoint.Deployment.SPIncludeSecurity]::All
	$settings.OverwriteExistingDataFile = 1
	$settings.ExcludeDependencies = $true

	$site = new-object Microsoft.SharePoint.SPSite($WebURL)
	#Write-Host "WebURL", $WebURL

	$web = $site.OpenWeb()
	#$list = $web.GetList($ListURL)
	foreach($list in $web.lists)
	{
		$settings.SiteUrl = $web.Url
		$exportObject.Id = $list.ID
		$newFolderPath = $ExportRootPath + $list.Title
		
		New-Item $newFolderPath -type directory -force
		
		$settings.FileLocation = $newFolderPath
		$settings.BaseFileName = "ExportList-"+ $list.ID.ToString() +".DAT"
		$settings.FileCompression = 1

		Write-Host "List", $list.Title
		Write-Host "FileLocation", $settings.FileLocation

		$settings.ExportObjects.Add($exportObject)

		$export = New-Object Microsoft.SharePoint.Deployment.SPExport($settings)
		$export.Run()
	}
	$web.Dispose()
	$site.Dispose()
}

Export-All-SPWeb-Lists "http://mySharePointWebApp/sites/mySiteCollection/" "C:\Temp\BackupRestoreTemp\"

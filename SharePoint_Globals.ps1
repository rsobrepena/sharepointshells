
#==============================================================
# Microsoft provides programming examples for illustration only, 
# without warranty either expressed or implied, including, but not 
# limited to, the implied warranties of merchantability and/or 
# fitness for a particular purpose. 
# This sample assumes that you are familiar with the programming 
# language being demonstrated and the tools used to create and debug 
# procedures. Microsoft support professionals can help explain the 
# functionality of a particular procedure, but they will not modify 
# these examples to provide added functionality or construct 
# procedures to meet your specific needs. if you have limited 
# programming experience, you may want to contact a Microsoft 
# Certified Partner or the Microsoft fee-based consulting line at 
# (800) 936-5200. 
# for more information about Microsoft Certified Partners, please 
# visit the following Microsoft Web site:
# https://partner.microsoft.com/global/30000104 
#
# Author: Tim Quinlan (tim.quinlan@microsoft.com)
#
# ----------------------------------------------------------
# History
# ----------------------------------------------------------

$farmprefix = $env:computername.Substring(0,4).ToLower();
$domain = (gwmi win32_computersystem | select -ExpandProperty domain).Split(".")[0];
$network = $env:computername.substring($env:computername.Length-1,1).ToLower();

if ( $farmprefix -eq "swab" )
{  $emsprefix = "shaw"; 
	 $altprefix = "shaw"; }
else
{
  $emsprefix = $farmprefix;
  $altprefix = $farmprefix;
}
$suffix = "";

if($network -eq "n")
{
  $suffix = "af.mil"
  $forestSuffix = "-";
}
else
{
   $suffix = "af.$($network)mil.mil";
   $relsuffix = "af.rel.$($network)mil.mil";
   $forestSuffix = "_";
}
#First three octet of IP
$ipAdd = (gwmi Win32_NetworkAdapterConfiguration | ?{$_.IPAddress -ne $null } ).IPAddress | select -Index 0
$ipMask = $ipAdd.Substring(0,$ipAdd.LastIndexOf("."));

if($network -eq "n")
{
  $rawXml = gc AIXConfiguration.xml;
  $permRaw = gc AFCENT_Permissions.xml
}
else
{
  $rawXml = gc AIXConfigurationSecure.xml;
  $permRaw = gc AFCENT_Permissions.xml
}

$formatXml = $rawXml -replace "@network",$network -replace "@domain", $domain -replace "@prefix",$farmprefix -replace "@suffix", $suffix -replace "@relsuffix", $relsuffix -replace "@ipmask", $ipMask -replace "@emsprefix", $emsprefix -replace "@altprefix", $altprefix;
$cfg = [xml]$formatXml


#=========================================
#   SharePoint 2013 Service Accounts
#=========================================
$farmaccount = $cfg.Farm.ServiceAccounts.FarmAccount.Username;
$crawlaccount = $cfg.Farm.ServiceAccounts.CrawlAccount.Username;
$apppoolaccount = $cfg.Farm.ServiceAccounts.AppPoolAccount.Username;
$relapppoolaccount = $cfg.Farm.ServiceAccounts.RelAppPoolAccount.Username;
$serviceaccount = $cfg.Farm.ServiceAccounts.ServicesAccount.Username;
$SuperUserAccount= $cfg.Farm.ServiceAccounts.SuperUser.Username;
$SuperReaderAccount=$cfg.Farm.ServiceAccounts.SuperReader.Username;
$profileAccount = $cfg.Farm.ServiceAccounts.ProfileAccount.UserName;
$passphrase = $cfg.Farm.StandardSettings.PassPhrase.Value;
$sslKey = $cfg.Farm.StandardSettings.SSLKey.Value;

#Service Application Pool settings
$saAppPoolName = $cfg.Farm.StandardSettings.ServiceAppPoolSettings.Name.Value;
$appPoolUserName = $cfg.Farm.StandardSettings.ServiceAppPoolSettings.UserName.Value;
if($network -eq "n")
{
	$emsAppPoolName = $cfg.Farm.ServiceAccounts.EMSAppPoolAccount.UserName;
}
#=========================================
#  Server Objects
#=========================================

$servers = $cfg.Farm.Servers.ContentServers.ContentServer

#SQL Server 
$databaseServerName  = $cfg.Farm.Servers.SQLServers.SQLServer.Name;
$activeClusterNode = Get-WMIObject -Class Win32_ComputerSystem -ComputerName $databaseServerName | Select -ExpandProperty Name

#==================================================
#  Web Applications
#==================================================

$webApps = $cfg.Farm.WebApplications.WebApplication;

#==================================================
#  Managed Paths Settings
#==================================================

$mps= $cfg.Farm.ManagedPaths.ManagedPath;


#==================================================================
#  CHANGE:  Add/Remove/Update database defaults as needed.
#==================================================================
#==================================================
#   Database Settings Constants	
#==================================================
if( $network -eq "n")
{
   $dbDataDefault = $cfg.Farm.StandardSettings.Database.Data.Path;
   $dbLogDefault = $cfg.Farm.StandardSettings.Database.Log.Path;
}
else
{
   $dbDataDefault = $cfg.Farm.StandardSettings.Database.Data.Path;
   $dbLogDefault = $cfg.Farm.StandardSettings.Database.Log.Path;
   $reldbDataDefault = $cfg.Farm.StandardSettings.Database.RelData.Path;
   $reldbLogDefault = $cfg.Farm.StandardSettings.Database.RelLog.Path
}
$mysiteData = $cfg.Farm.StandardSettings.Database.MySiteData.Path;
$mysiteLog = $cfg.Farm.StandardSettings.Database.MySiteLog.Path;
$dbfileGrowth = $cfg.Farm.StandardSettings.Database.DBGrowth.value;


#==================================================
#  Central Administration Settings
#==================================================
  $adminDB = $cfg.Farm.StandardSettings.CentralAdministration.DatabaseInfo.CentralAdminDB.Name;
  $configDB = $cfg.Farm.StandardSettings.CentralAdministration.DatabaseInfo.ConfigDB.Name;
  $farmDBDataPath =$cfg.Farm.StandardSettings.CentralAdministration.DatabaseInfo.DBData.Path;
  $farmDBLogPath =$cfg.Farm.StandardSettings.CentralAdministration.DatabaseInfo.DBLog.Path;
  $caPort = $cfg.Farm.StandardSettings.CentralAdministration.CAPort.Value;
  $caAuth = $cfg.Farm.StandardSettings.CentralAdministration.CAAuth.Value;

$farmDBCollection= $cfg.Farm.StandardSettings.CentralAdministration.DatabaseInfo;



#==================================================
#     Service Application Configuration
#==================================================


#Service Applicaion Names
$accesssSAName = "Access Services"
$bcsSAName = "Business Data Connectivity Service"
$excelSAName = "Excel Services Application"
$metadataSAName = "Managed Metadata Web Service"
$metadataSANameUS = "Managed Metadata Web Service USONLY"
$performancePointSAName = "PerformancePoint Service"
$searchSAName = "SharePoint Server Search Application"
$stateSAName = "State Service"
$secureStoreSAName = "Secure Store Service"
$usageSAName = "Usage and Health Data Collection Service"
$userProfileSAName = "User Profile Synchronization Service"
$visioSAName = "Visio Graphics Service"
$WebAnalyticsSAName = "Web Analytics Service"
$WordAutomationSAName = "Word Automation Services"
$WordViewingSAName = "Word Viewing Services"
$PowerPointSAName = "PowerPoint Services"

#Database Names
$serviceAppDBCollection= $cfg.Farm.ServiceApplications.ServiceApplication | ?{ $_.Databases -ne $null}
$searchDBPrefix = "$($farmprefix)_Search_2013";

#=======================================================
# Search settings
#=======================================================
$FrontEndServer1 = ($servers | ?{ $_.SearchRole -eq "Query1" }).Name;
$FrontEndServer2 = ($servers | ?{ $_.SearchRole -eq "Query2" }).Name;
$crawlServer = ($servers | ?{ $_.SearchRole -eq "Crawl" }).Name;

#=======================================================
# Outgoing email settings
#=======================================================
$smtpServer = $cfg.Farm.StandardSettings.Email.Server.Value
$smtpFrom = $cfg.Farm.StandardSettings.Email.From.Value
$smtpReply = $cfg.Farm.StandardSettings.Email.Reply.Value;

#=======================================================
# Other SharePoint Settings
#=======================================================
$hubURI =$cfg.Farm.StandardSettings.HubUrl.value;
$hubURIUS = $cfg.Farm.StandardSettings.HubUrlUS.value;
$mySiteHost = $cfg.Farm.StandardSettings.MySiteHost.Value
$upaFilter = "(|(&(objectclass=user)(useraccountcontrol=512)(!sAMAccountName=*.svc)(!sAMAccountName=*.adm)(!sAMAccountName=*.csa)(!sAMAccountName=sa.*)(!sAMAccountName=wm.*)(!sAMAccountName=HD.*)(!sAMAccountName=*.train)(!sAMAccountName=*.battlestaff)(!sAMAccountName=*.OPS)(!sAMAccountName=*.staff)(!sAMAccountName=csa.*))(&(objectclass=user)(useraccountcontrol=514))(&(objectclass=user)(useraccountcontrol=262656)))"
$upaDomains = @("nosc", "swab");
$srchUrl = $cfg.Farm.StandardSettings.SearchUrl.Value;
$syncConnections = $cfg.Farm.SyncConnections.SyncConnection;
$audiences = $cfg.Farm.Audiences.Audience;
$webAppPermissions = $cfg.Farm.StandardSettings.PermissionPolicy.Group;

#=======================================================
# Registry and other paths
#=======================================================
$iisLogPath = $cfg.Farm.StandardSettings.Paths.IISLog.Value;
$spLogPath = $cfg.Farm.StandardSettings.Paths.SPLogPath.Value;
$usageLogPath = $cfg.Farm.StandardSettings.Paths.usagePath.Value;
$blobCachePath= $cfg.Farm.StandardSettings.Paths.blobPath.Value;
$hostsFilePath = "C:\Windows\System32\drivers\etc\hosts";
#=======================================================
# Office Web Apps Server settings
#=======================================================
$intWACUrl=$cfg.Farm.StandardSettings.WAC.InternalUrl.Value;
$extWACUrl =$cfg.Farm.StandardSettings.WAC.ExternalUrl.Value;
$wacCertName  =$cfg.Farm.StandardSettings.WAC.CertName.Value;
$wacCacheLoaction =$cfg.Farm.StandardSettings.WAC.CacheLocation.Value;
$wacRenderLocation =$cfg.Farm.StandardSettings.WAC.RenderLocation.Value;
$wacServerName =$cfg.Farm.StandardSettings.WAC.ServerName.Value;

#=======================================================
#	ADFS Configuration
#=======================================================
$adfsSignInUrl = $cfg.Farm.StandardSettings.ADFSSettings.FederationUrl.value;
$adfsUrn = $cfg.Farm.StandardSettings.ADFSSettings.URN.value;
$adfsSignCertPath = $cfg.Farm.StandardSettings.ADFSSettings.SignCertPath.value;
$adfsRootCertPath = $cfg.Farm.StandardSettings.ADFSSettings.RootCertPath.value;
$adfsSecondaryRootCertPath = $cfg.Farm.StandardSettings.ADFSSettings.SecondaryRootCertPath.value;
$adfsName =$cfg.Farm.StandardSettings.ADFSSettings.IPName.value;

#=======================================================
#  Site Structure
#=======================================================
[xml]$siteXml = gc AFCENT_SiteStructure.xml;
$siteStructure = $siteXml.Webs.Web;
#=======================================================
# Archive Information
#=======================================================
$archiveSites = $cfg.Farm.SiteCollections.Archive.Site;

#=======================================================
# Permission Information
#=======================================================
$ownerMask = "ViewListItems, AddListItems, EditListItems, DeleteListItems, OpenItems, ViewVersions, ManageLists, ViewFormPages, Open, ViewPages, AddAndCustomizePages, ApplyThemeAndBorder, ApplyStyleSheets, CreateSSCSite, ManagePermissions, BrowseDirectories, BrowseUserInfo, UseClientIntegration, UseRemoteAPIs, CreateAlerts, EditMyUserInfo, EnumeratePermissions";
$contribMask = "ViewListItems, AddListItems, EditListItems, DeleteListItems, OpenItems, ViewVersions, DeleteVersions, ViewFormPages, Open, ViewPages, CreateSSCSite, ManagePermissions, BrowseDirectories, BrowseUserInfo, UseClientIntegration, UseRemoteAPIs, CreateAlerts, EditMyUserInfo, EnumeratePermissions";
$permFormat = $permRaw -replace "@network", $network;
$permissions = [xml]$permFormat;
$permWebs = $permissions.webs.web;
#=======================================================
# Error Strings
#=======================================================
$errorText1 =  $cfg.Farm.StandardSettings.Error.ErrorText1.Value;
$vMessage = $cfg.Farm.StandardSettings.Error.Prompt.Value;

#==============================================================
# Microsoft provides programming examples for illustration only, 
# without warranty either expressed or implied, including, but not 
# limited to, the implied warranties of merchantability and/or 
# fitness for a particular purpose. 
# This sample assumes that you are familiar with the programming 
# language being demonstrated and the tools used to create and debug 
# procedures. Microsoft support professionals can help explain the 
# functionality of a particular procedure, but they will not modify 
# these examples to provide added functionality or construct 
# procedures to meet your specific needs. if you have limited 
# programming experience, you may want to contact a Microsoft 
# Certified Partner or the Microsoft fee-based consulting line at 
# (800) 936-5200. 
# for more information about Microsoft Certified Partners, please 
# visit the following Microsoft Web site:
# https://partner.microsoft.com/global/30000104 
#
# Author: Tim Quinlan (tim.quinlan@microsoft.com)
#
# ----------------------------------------------------------
# History
# ----------------------------------------------------------


#load the global and helper functions
#get the location of the execution of this PS1 file
$path = Split-Path -parent $MyInvocation.MyCommand.Definition
. $path\SharePoint_Helper.ps1
. $path\SharePoint_Globals.ps1

$existingUrls = Get-SPAlternateUrl | select -expandproperty IncomingUrl;
$webApps | %{ 
		
		$waUrl = $_.Url;
		$eUrl = $existingUrls | ?{ $_.ToLower() -eq $waUrl.ToLower() }
		
		if ( $eUrl -eq $null)
		{
		Write-Log -Level Info  -Message "Creating New Web Application $($_.Name) at $($_.Url.ToLower())";
		$sfx="";
		if ( $_.IsExtended -eq $false )
		{	
			if($_.Url -like "*ems*" )
			{
				 $ap = New-SPAuthenticationProvider -DisableKerberos:$false;
				 $ssl = [System.Convert]::ToBoolean($_.SSL);
				 $anon = [System.Convert]::ToBoolean($_.Anonymous);

				 	New-SPWebApplication -Name $_.Name -Port $_.Port -URL $_.Url -Path $_.Path -ApplicationPool $_.AppPool -ApplicationPoolAccount (Get-SPManagedAccount $emsAppPoolName) `
			 		 -AuthenticationProvider $ap -SecureSocketsLayer:$ssl -AllowAnonymousAccess:$anon -HostHeader $_.Name -DatabaseName "EMS_DELETE"
					
				#Get a handle on the newly created web application
				$newApp = Get-SPWebApplication $_.Url
				if ( $newApp -ne $null )
				{
				   #set the time zone to GMT (Casablanca) 
				   $newApp.DefaultTimeZone = $_.TimeZone;

				   #set the Automatic Delete Unused site collections to false 
  				   #STIG SV-38109r1_rule
				   $newApp.AutomaticallyDeleteUnusedSiteCollections = $false
			
				   #Disable access to the Web Part catalog online STIG: SV-36596r1_rule
				   $newApp.AllowAccessToWebPartCatalog = $false;

				   #Update Max File Upload size to 2GB (2047 MB )
				   $newApp.MaximumFileSize = 2047;
				   $newApp.Update();
				}

			}#end EMS
			else
			{
				if( $network -ne "n")
				{
		   			$application = $_.Url; if($application -like "*.rel.*" ){ $sfx = "REL_" }else{ $sfx="US_" };
				}
				try
				{
					$ap = New-SPAuthenticationProvider -DisableKerberos:$false;
					$ssl = [System.Convert]::ToBoolean($_.SSL);
				 $anon = [System.Convert]::ToBoolean($_.Anonymous);
				 		New-SPWebApplication -Name $_.Name -Port $_.Port -URL $_.Url -Path $_.Path -ApplicationPool $_.AppPool -ApplicationPoolAccount (Get-SPManagedAccount $apppoolaccount) `
			 			 -AuthenticationProvider $ap -SecureSocketsLayer:$ssl -AllowAnonymousAccess:$anon -HostHeader $_.Name -DatabaseName "$($farmprefix)_PORTAL_HOME_$($sfx)CONTENT"
					
						#Create the root site
						Write-Log -Level Info  -Message "Creating root site collection at $($_.Url.ToLower())";
						New-SPSite -Url $_.Url -Template "STS#0" -OwnerAlias $farmaccount -Name "$($farmprefix) Portal" -ContentDatabase "$($farmprefix)_PORTAL_HOME_$($sfx)CONTENT";
			
				

					#Get a handle on the newly created web application
					$newApp = Get-SPWebApplication $_.Url
					if ( $newApp -ne $null )
					{
					   #set the time zone to GMT (Casablanca) 
					   $newApp.DefaultTimeZone = $_.TimeZone;

					   #set the Automatic Delete Unused site collections to false 
  					   #STIG SV-38109r1_rule
					   $newApp.AutomaticallyDeleteUnusedSiteCollections = $false
			
					   #Disable access to the Web Part catalog online STIG: SV-36596r1_rule
					   $newApp.AllowAccessToWebPartCatalog = $false;

					   #Update Max File Upload size to 2GB (2047 MB )
					   $newApp.MaximumFileSize = 2047;
					   $newApp.Update();
					}
				}
				catch
				{
						Write-Log -Level Warn -Message"An error has occurred";
						Write-Log -Level Warn -Message $_;
				}
			}#end non-ems
		}#end extended
		else
		{
			#Retrieve the web application to extend
			$parentWA= Get-SPWebApplication $_.ParentApp
			if( $parentWA -ne $null )
			{
			   $ap = New-SPAuthenticationProvider -DisableKerberos:$false;
			    $ssl = [System.Convert]::ToBoolean($_.SSL);
				 $anon = [System.Convert]::ToBoolean($_.Anonymous);
				try
				{
					
						 $parentWA | New-SPWebApplicationExtension -Name $_.Name -Port $_.Port -URL $_.Url -Path $_.Path  `
			 			-Zone $_.Zone -AuthenticationProvider $ap -SecureSocketsLayer:$ssl -AllowAnonymousAccess:$anon -HostHeader $_.Name
					
				}
				catch	
				{
					Write-Log -Level Error -Message "An error has occurred";
					Write-Log -Level Error -Message $_;
				}
			}
			else
			{
				Write-Log -Level Error -Message "$($_.ParentApp) web application cannot be found.";
			}
			
		}
		}
		else
		{ 
			Write-Log -Level Warn -Message "A web application or AAM with URL $($_.Url) already exists.";
		}
				
	 }

	 #Enable Blobcache
	 Write-Log -Level Info  -Message "Enabling Blobcache";
	 Get-SPWebApplication | Enable-SPBlobCache;

	 #Add Robots File
	 Write-Log -Level Info  -Message "Deploying Robots.txt files..";
	 $servers | %{ if( $_.Name -notlike "*app*" )
	 {
	   Deploy-RobotsFile -server $_.Name; } }

	 #Add SuperUser and SuperReader accounts
	 CreateObjectCacheAccountSettings;

	 #Add MSSService Account to Web Application 
	 Write-Log -Level Info -Message "Adding the $($serviceaccount) to each web application and setting File Handling";
	 Get-SPWebApplication | ?{ $_.Url -notlike "*ems*"} | %{ $_.GrantAccessToProcessIdentity( (Get-SPManagedAccount $serviceaccount).UserName); $_.BrowserFileHandling = [Microsoft.SharePoint.SPBrowserFileHandling]::Permissive; $_.Update();}

	 if( $network -ne "n" )
	 {
		 Write-Log -Level Info -Message "Remove "lnk" file from Block Extensions list in each web application";
		Get-SPWebApplication | ?{ $_.Url -notlike "*ems*"} | %{ $_.BlockedFileExtensions.Remove("lnk"); $_.Update(); }
	 }
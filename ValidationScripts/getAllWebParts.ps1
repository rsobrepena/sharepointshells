﻿param(
    [string]$siteUrl,
    [string]$fileLocation,
    [string]$fileName
)

Add-PSSnapin Microsoft.SharePoint.PowerShell

try{

$date = Get-Date

if(!$fileName){
    $fileName = "WebPartReport-" + $date.Month + "." + $date.Day + "." + $date.Year + ".csv"
}else{
    if(!$fileName.EndsWith(".csv")){
        $fileName = $fileName + ".csv"
    }

}

if(!$fileLocation.EndsWith("\")){
    $fileLocation = $fileLocation + "\"
}

$fileFullPath = $fileLocation + $fileName

$table = New-Object System.Data.DataTable "AllWebParts"
$colDisplayName = New-Object System.Data.DataColumn DisplayName,([string])
$colDescription = New-Object System.Data.DataColumn Description,([string])
$colUrl = New-Object System.Data.DataColumn Url,([string])
$table.Columns.Add($colDisplayName)
$table.Columns.Add($colDescription)
$table.Columns.Add($colUrl)

$site = Get-SPSite $siteUrl
$web = $site.OpenWeb()
$webPartGallery = $web.Lists["Web Part Gallery"]
$items = $webPartGallery.Items
foreach($item in $items){
    
    $descSource = $item["Description"]
    $desc =[Microsoft.SharePoint.Utilities.SPUtility]::GetLocalizedString($descSource, "core",1033)  
    
    $row = $table.NewRow()
    $row.DisplayName = $item.DisplayName
    $row.Description = $desc
    $row.Url = $item.Url
    $table.Rows.Add($row)
}

$table | Export-Csv $fileFullPath
$site.Dispose()

} catch [Exception]{
    return $_.Exception.Message
}

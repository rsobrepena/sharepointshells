param([string]$rootUrl)
[System.Reflection.Assembly]::LoadWithpartialName("microsoft.sharepoint")

Remove-Item AFCENT_Permissions.xml -EA 0

function WriteXml($str)
{
   $str | out-file AFCENT_Permissions.xml -append
}

function EnumPerms($web)
{

   Write-Host "Current Web is $($web.Url)";
   $gText= ""; 
   $web.RoleAssignments | ?{ ($_.Member.Name -like "*owner*") -or ($_.Member.Name -like "*contributor*") } | %{ 
	$_.Member.Users | ?{ ($_.IsDomainGroup -eq $true) -and ($_.LoginName -notlike "*singlesign*") -and (($_.LoginName -like "*_ow*") -or ($_.LoginName -like "*_co*") )  }  | %{ $gText += "<Member Login='$($_.LoginName)' />`n"; }
	}
   
   foreach($sWeb in $web.Webs)
   {
	EnumPerms($sWeb);
   }
   if($gText -ne "" )
   {
	$wUrl = $web.ServerRelativeUrl.Replace("/shared","").Replace("/public","").Replace("/private","").Replace("/e/","/").Replace("/l/","/");
 
   WriteXml("<Web Url='$($wUrl)'>");
   WriteXml($gText);
   WriteXml("</Web>");
   }
   $gText= "";
   $web.Dispose();

}#endfunction

WriteXml("<webs>");

#AFCENT
$site = new-object Microsoft.SharePoint.SPSite("$($rootUrl)/e/afcent");
$rWeb = $site.OpenWeb();
EnumPerms($rWeb);
$rWeb.Dispose();
#CFACC
$site = new-object Microsoft.SharePoint.SPSite("$($rootUrl)/e/cfacc");
$rWeb = $site.OpenWeb();
EnumPerms($rWeb);
$rWeb.Dispose();
#9AETF
$site = new-object Microsoft.SharePoint.SPSite("$($rootUrl)/e/9aetf");
$rWeb = $site.OpenWeb();
EnumPerms($rWeb);
$rWeb.Dispose();
WriteXml("</webs>");

Write-Host "Complete - Please copy AFCENT_Permissions.xml to the SharePoint 2013 D:\Build\PowerShell scripts folder";

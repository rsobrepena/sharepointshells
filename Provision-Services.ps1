
#==============================================================
# Microsoft provides programming examples for illustration only, 
# without warranty either expressed or implied, including, but not 
# limited to, the implied warranties of merchantability and/or 
# fitness for a particular purpose. 
# This sample assumes that you are familiar with the programming 
# language being demonstrated and the tools used to create and debug 
# procedures. Microsoft support professionals can help explain the 
# functionality of a particular procedure, but they will not modify 
# these examples to provide added functionality or construct 
# procedures to meet your specific needs. if you have limited 
# programming experience, you may want to contact a Microsoft 
# Certified Partner or the Microsoft fee-based consulting line at 
# (800) 936-5200. 
# for more information about Microsoft Certified Partners, please 
# visit the following Microsoft Web site:
# https://partner.microsoft.com/global/30000104 
#
# Author: Tim Quinlan (tim.quinlan@microsoft.com)
#
# ----------------------------------------------------------
# History
# ----------------------------------------------------------

param([switch]$accessServices,[switch]$bcsServices,[switch]$excelServices,[switch]$perfpointServices,[switch]$searchService2010,[switch]$searchService2013,[switch]$stateServices,
	  [switch]$profileServices,[switch]$visioServices,[switch]$secureServices,[switch]$analyticServices,[switch]$wordViewingServices,[switch]$powerPTService,
	  [switch]$usageServices,[switch]$metadataServices,[switch]$sipr,[switch]$nipr,[switch]$createTerms,[switch]$metadataServicesUS)
	  

#load the global and helper functions
#get the location of the execution of this PS1 file
$path = Split-Path -parent $MyInvocation.MyCommand.Definition
. $path\SharePoint_Helper.ps1
. $path\SharePoint_Globals.ps1

Remove-PSSnapin Microsoft.SharePoint.PowerShell -erroraction SilentlyContinue
Add-PSSnapin Microsoft.SharePoint.PowerShell -erroraction SilentlyContinue

if($all)
{
   Write-Log -Level Info  -Message "All SharePoint Service Applications will be created.";
   $ans = Read-Host "Do you want to continue?(y/n)";
}
if( $ans -eq "n" )
{
  Write-Log -Level Info  -Message "Exiting Provision Services...";
  exit;
}

#===================================================
#  Application Pool Setup
#===================================================
$saAppPool = Get-SPServiceApplicationPool -Identity $saAppPoolName -EA 0
if($saAppPool -eq $null)
{
	Write-Log -Level Info  -Message "Creating Service Application Pool..."
	$appPoolAccount = Get-SPManagedAccount -Identity $appPoolUserName -EA 0
    if($appPoolAccount -eq $null)
    {
		Write-Log -Level Info  -Message "Please supply the password for the Service Account $($appPoolUserName)";
		$appPoolCred = Get-Credential $appPoolUserName
		$appPoolAccount = New-SPManagedAccount -Credential $appPoolCred -EA 0
	}
	$appPoolAccount = Get-SPManagedAccount -Identity $appPoolUserName -EA 0
	if($appPoolAccount -eq $null )
	{
		Write-Log -Level Info  -Message "Cannot create or find the managed account $appPoolUserName, please ensure the account exists."
		Exit -1
	}
	else 
	{
		New-SPServiceApplicationPool -Name $saAppPoolName -Account $appPoolAccount -EA 0 > $null
	}
}


#=========================================
# Usage and Health Service Application
#=========================================
if( $usageServices )
{
	#Get the Database info
		$dbInfo = $serviceAppDBCollection | ?{ $_.ServiceName -eq $usageSAName }
	#Create log directory
	$logPath = "\\$($activeClusterNode)\w$\sql\log";
	Write-Log -Level Info  -Message "Creating directories $($logPath)";
	New-Item -Path $logPath -Type Directory -EA 0 -Force
	#Create the Service Application
	Write-Log -Level Info  -Message "Creating Usage Service Application $($usageSAName)"
		$serviceInstance = Get-SPUsageService
		New-SPUsageApplication -Name $usageSAName -DatabaseServer $databaseServerName -DatabaseName $dbInfo.Databases.Database.DBName -UsageService $serviceInstance
	Write-Log -Level Info  -Message "Provisioning Usage and Health proxy";
        (Get-SPServiceApplicationProxy | ?{ $_.DisplayName -like "*$($usageSAName)*"}).Provision();
	#Set the logging directory
	Write-Log -Level Info  -Message "Complete" ;
	Set-SPUsageService -UsageLogLocation $usageLogPath
	Write-Log -Level Info  -Message "Complete" ;


}

#===========================================
#  Access Service Application
#===========================================
if( $accessServices )
{
	Write-Log -Level Info  -Message "Creating Access Services and Proxy..."	
	New-SPAccessServiceApplication -Name $accesssSAName -ApplicationPool $saAppPoolName > $null
	Get-SPServiceInstance | where-object {$_.TypeName -eq "Access Services"} | Start-SPServiceInstance > $null
}

#=========================================
#  Excel Service Application
#=========================================

if( $excelServices )
{
	Write-Log -Level Info  -Message "Creating Excel Service..."
	New-SPExcelServiceApplication -name $excelSAName –ApplicationPool $saAppPoolName > $null
	Get-SPServiceInstance | where-object {$_.TypeName -eq "Excel Services Application" -And $_.Server -like "*app*"} | Start-SPServiceInstance
	try
	{
		Set-SPExcelFileLocation -Identity "http://" -ExcelServiceApplication $excelSAName -ExternalDataAllowed 2 -WorkbookSizeMax 10 -WarnOnDataRefresh:$true -EA "Stop"
		New-SPExcelFileLocation -Address "https://" -ExcelServiceApplication $excelSAName -ExternalDataAllowed 2 -WorkbookSizeMax 10 -WarnOnDataRefresh:$true -LocationType SharePoint -UdfsAllowed:$true -EA "Stop"
	}
	catch
	{  Write-Log -Level Error -Message "There was an issue adding the Trusted Excel File Location:$($_.ToString())"; }
}

#========================================
# MMS 
#========================================

if($metadataServices )
{
	#Create the Database
		$dbInfo = $serviceAppDBCollection | ?{ $_.ServiceName -eq $metadataSAName }
		Write-Log -Level Info  -Message "Creating Database $($dbInfo.Databases.Database.DBName) for $($metadataSAName)";
		Create-SPDatabaseOnSQL -dbDataDefault $dbInfo.Databases.Database.DataPath -dbLogDefault $dbInfo.Databases.Database.LogPath -farmaccount $farmaccount -databaseName $dbInfo.Databases.Database.DBName
		Start-Sleep 10;
	if( Test-SPSite $hubURI )
	{
	Get-SPServiceInstance | where-object {$_.TypeName -eq "Managed Metadata Web Service"} | Start-SPServiceInstance > $null
	Start-Sleep 5;
	Write-Log -Level Info  -Message "Creating Metadata Service and Proxy..."
		New-SPMetadataServiceApplication -Name $metadataSAName -ApplicationPool $saAppPoolName -DatabaseServer $databaseServerName `
        -DatabaseName $dbInfo.Databases.Database.DBName -HubUri $hubUri
		Start-Sleep 5;
    New-SPMetadataServiceApplicationProxy -Name "$($metadataSAName) Proxy" -DefaultProxyGroup -ServiceApplication $metadataSAName `
	-ContentTypePushdownEnabled -DefaultSiteCollectionTaxonomy -ContentTypeSyndicationEnabled -DefaultKeywordTaxonomy > $null
	
    
		Write-Log -Level Info  -Message "Adding Default AFCENT managed metadata";
       . .\Create-Terms.ps1 -siteUrl $hubUri -filename "AFCENTTerms.xml"
	}
	else
	{
	  Write-Log -Level Error  -Message "Cannot locate SPSite $($hubURI) - Please verify this site exists and retry"
	}
    
}
#========================================
# MMS -US only
#========================================
if($metadataServicesUS )
{
	#Create the Database
		$dbInfo = $serviceAppDBCollection | ?{ $_.ServiceName -eq $metadataSANameUS }
		Write-Log -Level Info  -Message "Creating Database $($dbInfo.Databases.Database.DBName) for $($metadataSANameUS)";
		Create-SPDatabaseOnSQL -dbDataDefault $dbInfo.Databases.Database.DataPath -dbLogDefault $dbInfo.Databases.Database.LogPath -farmaccount $farmaccount -databaseName $dbInfo.Databases.Database.DBName
		Start-Sleep 10;
	if( Test-SPSite $hubURIUS )
	{
		Get-SPServiceInstance | where-object {$_.TypeName -eq "Managed Metadata Web Service"} | Start-SPServiceInstance > $null
		Start-Sleep 5;
		Write-Log -Level Info  -Message "Creating Metadata Service and Proxy..."
		New-SPMetadataServiceApplication -Name $metadataSANameUS -ApplicationPool $saAppPoolName -DatabaseServer $databaseServerName `
        -DatabaseName $dbInfo.Databases.Database.DBName -HubUri $hubUriUS
		Start-Sleep 5;
		New-SPMetadataServiceApplicationProxy -Name "$($metadataSANameUS) Proxy" -DefaultProxyGroup -ServiceApplication $metadataSANameUS `
		-ContentTypePushdownEnabled -DefaultSiteCollectionTaxonomy -ContentTypeSyndicationEnabled -DefaultKeywordTaxonomy > $null
	
    
		Write-Log -Level Info  -Message "Adding Default AFCENT managed metadata";
       . .\Create-Terms.ps1 -siteUrl $hubUriUS -filename "AFCENTTerms.xml"
	}
	else
	{
	  Write-Log -Level Error  -Message "Cannot locate SPSite $($hubURIUS) - Please verify this site exists and retry"
	}
    
}

#===========================================================
#  Search Service 2013 - not for SharePoint 2010  
#===========================================================
if( $searchService2013 )
{
	
	$ans = Read-Host "Q: Do you want to pre-create the search databases?(y/n)";
	if( $ans -eq "y")
	{
		#Create the Database
		$dbInfo = $serviceAppDBCollection | ?{ $_.ServiceName -eq $searchSAName }
		$dbInfo.Databases.Database | %{ 
			Write-Log -Level Info  -Message "Creating Database $($_.Name) for $($searchSAName)";
		Create-SPDatabaseOnSQL -dbDataDefault $_.DataPath -dbLogDefault $_.LogPath -farmaccount $farmaccount -databaseName $_.DBName
	}
	Start-Sleep 10;
	}
	

	$IndexLocation = "\\{0}\d$\SearchIndex\";
	Add-PSSnapin Microsoft.SharePoint.PowerShell -erroraction SilentlyContinue
	##START SEARCH

	#Get the Servers in question
	$searchServers = $servers | ?{ $_.SearchRole -ne $null };
	$searchServers | %{ 

	    if( $_.SearchRole -like "Query*" )
		{
		  if((Get-SPEnterpriseSearchServiceInstance $_.Name | select -expandproperty Status) -ne "Online")
		  {
		  	 Write-Log -Level Info  -Message "Starting Search Services on $($_.Name)";
		  	 Start-SPEnterpriseSearchServiceInstance $_.Name;
			 $status = Get-SPEnterpriseSearchServiceInstance $_.Name | select -expandproperty Status
			 do{
				$status = Get-SPEnterpriseSearchServiceInstance $_.Name | select -expandproperty Status
				Write-Host "." -nonewline; Start-Sleep 3;
				}
				while($status -ne "Online")
			    Verify;
                  }
		  if((Get-SPEnterpriseSearchServiceInstance $_.Name | select -expandproperty Status) -ne "Online")
		  {
			 Write-Log -Level Info  -Message "Starting Query Services on $($_.Name)";
		  	Start-SPEnterpriseSearchQueryAndSiteSettingsServiceInstance $_.Name;
			Verify;
		  }	
		 
		}
		if( $_.SearchRole -eq "Crawl")
		{
		  #Add Crawl Account to local administrators group on crawl servers
		  Add-LocalAdministrator -doamin $crawlaccount.Split('\')[0] -userName $crawlaccount.Split('\')[1] -computername $_.Name
		  if((Get-SPEnterpriseSearchServiceInstance $_.Name | select -expandproperty Status) -ne "Online")
		  {
		  	 Write-Log -Level Info  -Message "Starting Services on $($_.Name)";
		  	Start-SPEnterpriseSearchServiceInstance $_.Name;

			 $status = Get-SPEnterpriseSearchServiceInstance $_.Name | select -expandproperty Status
			 do{
				$status = Get-SPEnterpriseSearchServiceInstance $_.Name | select -expandproperty Status
				Write-Host "." -nonewline; Start-Sleep 3;
				}
				while($status -ne "Online")
			Verify;
                  }
		}
	  
	}
	$ans = Read-Host "Do you want to install the Search Service Application? (y/n)";
        if( $ans -eq "y" )
        {
	Write-Log -Level Warn -Message "Checking if Search Service Application exists";
	$ServiceApplication = Get-SPEnterpriseSearchServiceApplication -Identity $searchSAName -ErrorAction SilentlyContinue
	if ($ServiceApplication -eq $null)
	{
		Write-Log -Level Info  -Message  "Creating Search Service Application"
		$ServiceApplication = New-SPEnterpriseSearchServiceApplication -Name $searchSAName -ApplicationPool $saAppPoolName -DatabaseServer  $databaseServerName -DatabaseName $searchDBPrefix -Partitioned;
	}
 	Start-Sleep 10;
	Write-Log -Level Warn -Message  "Checking if Search Service Application Proxy exists";
	$Proxy = Get-SPEnterpriseSearchServiceApplicationProxy -Identity "$($searchSAName) Proxy"  -ErrorAction SilentlyContinue
	if ($Proxy -eq $null)
	{
		Write-Log -Level Info  -Message  "Creating Search Service Application Proxy"
		New-SPEnterpriseSearchServiceApplicationProxy -Name "$($searchSAName) Proxy" -SearchApplication $ServiceApplication 
	}
    
	Verify;
	}
	$ans = Read-Host "Do you want to create the new topology for the Search Service Application? (y/n)";
        if( $ans -eq "y" )
        {
	$ServiceApplication = Get-SPEnterpriseSearchServiceApplication -Identity $searchSAName -ErrorAction SilentlyContinue
	
    Write-Log -Level Info  -Message "Creating new topology...";
	$InitialSearchTopology = $ServiceApplication | Get-SPEnterpriseSearchTopology -Active 
	$SearchTopology = $ServiceApplication | New-SPEnterpriseSearchTopology
	$searchServers | %{ 
	    
	    if( $_.SearchRole -like "Query*" )
		{
		  Write-Log -Level Info  -Message "Starting  Query Services on $($_.Name)";
		  $searchInstance = Get-SPEnterpriseSearchServiceInstance $_.Name
		  New-SPEnterpriseSearchQueryProcessingComponent -SearchTopology $SearchTopology -SearchServiceInstance $searchInstance
		}
		if( $_.SearchRole -eq "Crawl")
		{
		   Write-Log -Level Info  -Message "Starting Crawl Services on $($_.Name)";
		   $searchInstance = Get-SPEnterpriseSearchServiceInstance $_.Name
		   New-SPEnterpriseSearchAnalyticsProcessingComponent -SearchTopology $SearchTopology -SearchServiceInstance $searchInstance
		   New-SPEnterpriseSearchContentProcessingComponent -SearchTopology $SearchTopology -SearchServiceInstance $searchInstance
	       New-SPEnterpriseSearchCrawlComponent -SearchTopology $SearchTopology -SearchServiceInstance $searchInstance 
		   New-SPEnterpriseSearchAdminComponent -SearchTopology $SearchTopology -SearchServiceInstance $searchInstance
		   $idx = $IndexLocation.Replace("{0}",$_.Name);
		   Write-Log -Level Info -Message "Clearing out $($idx) to prepare for index location";
		   if ( (Test-Path $idx) )
		   {
		      Remove-Item -Recurse -Force -LiteralPath $idx  -ErrorAction SilentlyContinue
			  
		   }
		   Remove-Item -Recurse -Force -LiteralPath "D:\SearchIndex"  -ErrorAction SilentlyContinue
		   New-Item -Path $idx -Type Directory -Force
		   New-Item -Path "D:\SearchIndex" -Type Directory -Force
		   Write-Log -Level Info -Message "Clearing local SearchIndex folder";
		   if( $_.Name -ne $env:computername)
		   {
		       Write-Log -Level Info  -Message "Creating index component remotely on $($_.Name) ";
         	   New-SPEnterpriseSearchIndexComponent -SearchTopology $SearchTopology -SearchServiceInstance $searchInstance -RootDirectory "D:\SearchIndex" -IndexPartition 0;
       			
			 
           	 	#Set-SPEnterpriseSearchServiceInstance -Identity $searchInstance  –DefaultIndexLocation D:\SearchIndex;
		   }
		   else
		   {
			New-SPEnterpriseSearchIndexComponent -SearchTopology $SearchTopology -SearchServiceInstance $searchInstance -RootDirectory D:\SearchIndex -IndexPartition 0;
		   }
 
		}

	}
   
	
	Set-SPEnterpriseSearchAdministrationComponent -SearchApplication $ServiceApplication -SearchServiceInstance  $searchInstance
	Write-Log -Level Info  -Message "Setting New Topology";
    	Set-SPEnterpriseSearchTopology -Identity $SearchTopology
		Verify;
    }
    #Create the ContentSources
    $ans = Read-Host "Do you want to add the Search Content Sources? (y/n)";
    if( $ans -eq "y" )
    {
    if ( $ServiceApplication  -eq $null )
    {
	    $ServiceApplication = Get-SPEnterpriseSearchServiceApplication;
    }
    Write-Log -Level Info  -Message "Creating Content Source Connections";
    Write-Log -Level Info  -Message "Setting default connections"

	#Get Start Addresses 
	$webApps | ?{ ($_.IsExtended -eq $false) -and ($_.Name -notlike "*ems*") } | %{  $startAddress += "$($_.Url);"; $peopleSearchAddress = "sps3s://$($_.Name)"; }
    $ls = Get-SPEnterpriseSearchCrawlContentSource -SearchApplication $ServiceApplication -Identity "Local SharePoint sites";
    $ls | Set-SPEnterpriseSearchCrawlContentSource -StartAddresses $startAddress.TrimEnd(";") -ScheduleType Incremental -DailyCrawlSchedule -CrawlScheduleRepeatInterval 60 -CrawlScheduleRepeatDuration 1440;
    #Add People Search
	Write-Log -Level Info  -Message "Adding People Search Content Source";
    New-SPEnterpriseSearchCrawlContentSource -SearchApplication $ServiceApplication -Name "People" -Type SharePoint -StartAddresses $peopleSearchAddress;
    $ps = Get-SPEnterpriseSearchCrawlContentSource -SearchApplication $ServiceApplication -Identity "People";
    $ps | Set-SPEnterpriseSearchCrawlContentSource -ScheduleType Incremental -DailyCrawlSchedule -CrawlScheduleRepeatInterval 180 -CrawlScheduleRepeatDuration 1440;
    
    Verify;
    }
	$sa = Get-SPEnterpriseSearchServiceApplication;
	$sa.SearchCenterUrl = "/search/pages";
	$sa.Update();

	#Set up the default content access account
	Write-Log -Level Info  -Message "Adding the default access account" ;
	Write-Host
	Write-Log -Level Info  -Message "Please enter the password for $($crawlaccount)...";
	$cred = Get-Credential $crawlaccount;
	Get-SPEnterpriseSearchServiceApplication | Set-SPEnterpriseSearchServiceApplication -DefaultContentAccessAccountName $cred.UserName -DefaultContentAccessAccountPassword $cred.Password 
	Write-Log -Level Info  -Message "Setting Noderunner and Host controller to crawl account" ;
	Write-Host
	Set-SPEnterpriseSearchService -ServiceAccount $cred.UserName -ServicePassword $cred.Password;
	#Create Search Center
	if( !(Get-SPWeb $srchUrl -EA 0).Exists )
	{
	   # Template SRCHCEN#0
	    $site = get-spsite $srchUrl.Replace("/search","");
		if ($site.Features["f6924d36-2fa8-4f0b-b16d-06b7250180fa"] -eq $null )
		{
			Write-Host "Activating Publishing site Feature on $($site.Url)";
			$g = new-object System.Guid("f6924d36-2fa8-4f0b-b16d-06b7250180fa");
			$site.Features.Add($g);
		}
	   New-SPWeb -Url $srchUrl -Template "SRCHCEN#0" -Name "USAFCENT Search Center";
	}
}

if( $stateServices )
{
	
	#Create the Database
	$dbInfo = $serviceAppDBCollection | ?{ $_.ServiceName -eq $stateSAName }
	Write-Log -Level Info  -Message "Creating State Service and Proxy..."
	New-SPStateServiceDatabase -Name $dbInfo.Databases.Database.DBName -DatabaseServer $databaseServerName | New-SPStateServiceApplication -Name $stateSAName | New-SPStateServiceApplicationProxy -Name "$stateSAName Proxy" -DefaultProxyGroup > $null
	Enable-SPSessionStateService -DatabaseName "$($dbInfo.Databases.Database.DBName)_ASPSession"

	#Update the Web Config files to allow ASP.NET session
	Write-Log -Level Info -Message "Enabling Session state in Web.Config files";
	Get-SPWebApplication | %{ Enable-ASPSession -WebApplication $_; Start-Sleep 30; }
	
}
if( $profileServices )
{
	#Comment out for upgrade
	Write-Log -Level Info  -Message "Creating User Profile Service and Proxy..."

	#Databases
	$ans = Read-Host "Q: Do you want to pre-create the profile databases?(y/n)";
	$dbInfo = $serviceAppDBCollection | ?{ $_.ServiceName -eq $userProfileSAName }
	$ProfileServiceDB = $dbInfo.Databases.Database |?{ $_.DBName -like "*Profile*" } | select -ExpandProperty DBName
	$SocialServiceDB = $dbInfo.Databases.Database |?{ $_.DBName -like "*Social*" } | select -ExpandProperty DBName
	$SyncServiceDB = $dbInfo.Databases.Database |?{ $_.DBName -like "*Sync*" } | select -ExpandProperty DBName

	if( $ans -eq "y")
	{
		#Create the Database	
		$dbInfo.Databases.Database | %{ 
			Write-Log -Level Info  -Message "Creating Database $($_.Name) for $($userProfileSAName)";
		Create-SPDatabaseOnSQL -dbDataDefault $_.DataPath -dbLogDefault $_.LogPath -farmaccount $farmaccount -databaseName $_.DBName
		}
	}
	Start-Sleep 10;
	#Create MySite Host
	$ans = Read-Host "Q: Do you want to create the MySite Host?(y/n)";
	if( $ans -eq "y")
	{
		if( !(Test-SPSite $mySiteHost -EA 0).Exists )
		{
		   New-SPSite -Url $mySiteHost -Name "USAFCENT My Sites" -Template SPSMSITEHOST#0  -OwnerAlias $farmaccount
		   #Disable the First Time User Experience
		   $web = Get-SPWeb $mySiteHost;
		   $web.Properties["urn:schemas-microsoft-com:sharepoint:portal:profile:SPS-O15FirstRunExperience"]="OFF";
		   $web.Update();
		   $web.Dispose();
		}	
		#enable self service site creation on host web application
		(Get-SPSite $mySiteHost).WebApplication.SelfServiceSiteCreationEnabled = $true;
		(Get-SPSite $mySiteHost).WebApplication.Update();
	}
	$ans = Read-Host "Q: Do you want to create the provision the User Profile Service Application(y/n)";
	if( $ans -eq "y")
	{
		Start-Sleep 10;
		Write-Log -Level Info  -Message "Provisioning User Profile Service Application";
		$userProfileService = New-SPProfileServiceApplication -Name $userProfileSAName -ApplicationPool $saAppPoolName -ProfileDBServer $databaseServerName -ProfileDBName $ProfileServiceDB -SocialDBServer $databaseServerName -SocialDBName $SocialServiceDB -ProfileSyncDBServer $databaseServerName -ProfileSyncDBName $SyncServiceDB -MySiteHostLocation $mySiteHost -MySiteManagedPath "personal"
		New-SPProfileServiceApplicationProxy -Name "$userProfileSAName Proxy" -ServiceApplication $userProfileService -DefaultProxyGroup > $null
		$syncMachine = $servers | ?{ $_.Sync -eq $true } | select -expandproperty name 
		$syncServer = Get-SPServer $syncMachine;
		$si = Get-SPServiceInstance | where-object {($_.TypeName -eq "User Profile Service") -and ( $_.Server -like "*$($syncMachine)*" )} 
		$si | Start-SPServiceInstance
		 $status = Get-SPServiceInstance $_.Name | select -expandproperty Status
				 do{
					$status = Get-SPServiceInstance $si.Id | select -expandproperty Status
					Write-Host "." -nonewline; Start-Sleep 3;
					}
					while($status -ne "Online")
	   $sa = Get-SPServiceApplication | ?{ $_.Name -like "*$($userProfileSAName)*" }
	   $syncInstance = Get-SPServiceInstance -Server $syncMachine | ?{ $_.TypeName -eq "User Profile Synchronization Service"}
	   $syncInstance.Status = [Microsoft.SharePoint.Administration.SPObjectStatus]::Provisioning
		$syncInstance.IsProvisioned = $false
		$syncInstance.UserProfileApplicationGuid = $sa.Id
		$syncInstance.Update()
		Write-Log -Level Info  -Message "Please enter the password for $($farmaccount).";
		$account = Get-Credential $farmaccount
		$sa.SetSynchronizationMachine($syncServer.Address, $syncInstance.Id, $account.UserName,  $account.Password)
		#Check the status and don't return until the process has started
		Write-Log -Level Info  -Message "Starting Synchronization Services on $($syncMachine)";
		$syncInstance = Get-SPServiceInstance -Server $syncMachine | ?{ $_.TypeName -eq "User Profile Synchronization Service"}
		$status = $syncInstance.Status
		 do{
					$status = Get-SPServiceInstance $syncInstance.Id | select -expandproperty Status
					Write-Host "*" -nonewline; Start-Sleep 3;
					}
					while($status -ne "Online")
	   $sa = Get-SPServiceApplication | ?{ $_.Name -like "*$($userProfileSAName)*" }
	   $sa.NetBiosDomainNamesEnabled = $true;
	   $sa.Update();
	   #Enable the the timer job
	   Write-Log -Leve Info -Message "Enabling the User Profile Import Timer Job";
	   $upaJob = Get-SPTimerJob | ?{ $_.Name -like "*profileimport*" }
	   $upaJob.IsDisabled = $false;
	   $upaJob.Update();
	   #Do IISRESET
	   & iisreset
	}
	   $ans = Read-Host "Q: Do you want to create the audiences for the User Profile Service Application(y/n)";
	   if( $ans -eq "y")
	   {
			$siteUrl = $mySiteHost.Replace("/sites/my","");
			$audiences | %{	
				
				Create-SPAudience -Name "$($_.Name) Users" -Site $siteUrl -Rules $_.Rule;
			}
	   }
 
   
   #Grant permissions
   Write-Log -Level Info -Message "Granting permission for Crawl Account for People Search";
   Add-ServiceApplicationPermission -username $crawlaccount -permission "Retrieve People Data for Search Crawlers" -serviceAppName $userProfileSAName -admin
   Write-Log -Level Info -Message "Granting permission for Service Application App Pool Account";
   Add-ServiceApplicationPermission -username $serviceaccount -permission "Full Control" -serviceAppName $userProfileSAName
   Add-ServiceApplicationPermission -username $apppoolaccount -permission "Full Control" -serviceAppName $userProfileSAName
   if( $network -ne "n")
   {
	Add-ServiceApplicationPermission -username $relapppoolaccount -permission "Full Control" -serviceAppName $userProfileSAName
   }

   #Create Connections
   <#
   $ans = Read-Host "Q: Do you want to create the sync connections for the User Profile Service Application(y/n)";
   if( $ans -eq "y")
   {
      Create-UPASyncConnections

   }#>
}
if( $visioServices )
{
	Write-Log -Level Info  -Message "Creating Visio Graphics Service and Proxy..."
	New-SPVisioServiceApplication -Name $visioSAName -ApplicationPool $saAppPoolName > $null
	New-SPVisioServiceApplicationProxy -Name "$visioSAName Proxy" -ServiceApplication $visioSAName > $null
	Get-SPServiceInstance | where-object {$_.TypeName -eq "Visio Graphics Service"} | Start-SPServiceInstance > $null
}


#===================================================
#  Service Application Association
#===================================================
if( $network -ne "n" )
{
	$pr = Get-SPServiceApplicationProxyGroup "USONLY" -EA 0;
	if( $pr -eq $null )
	{
		#Create a new Service Application Proxy Group;
		New-SPServiceApplicationProxyGroup -Name "USONLY";
	}
	#Get the Service Application Proxies
	$proxies = Get-SPServiceApplicationProxy | ?{ $_.DisplayName -like "*usonly*" -or $_.DisplayName -like "*excel*" -or $_.DisplayName -like "*profile*" };
	if( $proxies.Count -gt 0 )
	{
	  $proxies | %{ Write-Log -Level Info -Message "Adding $($_.DisplayName) to Proxy Group USONLY"; Add-SPServiceApplicationProxyGroupMember -Identity "USONLY" -Member $_; 
	  if( $_.DisplayName -like "*usonly*"){ Get-SPServiceApplicationProxyGroup | ?{ $_.FriendlyName -like "*default*" } | Remove-SPServiceApplicationProxyGroupMember -Member $_; 
	  } }
	}
	  Write-Log -Level Info -Message "Setting the USONLY web application to use the new services proxy group.";
	  Get-SPWebApplication | ?{ $_.Url -notlike "*.rel.*" } | Set-SPWebApplication -ServiceApplicationProxyGroup "USONLY";
}

#Add Excel and Access Service to Default Group;
$proxies = Get-SPServiceApplicationProxy | ?{ $_.DisplayName -like "*access*" -or $_.DisplayName -like "*excel*" };
if( $proxies.Count -gt 0 )
{
	 $pg = Get-SPServiceApplicationProxyGroup | ?{ $_.FriendlyName -like "*default*" };
	 $proxies | %{ Write-Log -Level Info -Message "Adding $($_.DisplayName) to Proxy Group Default"; Add-SPServiceApplicationProxyGroupMember -Identity $pg -Member $_; }
	 
}
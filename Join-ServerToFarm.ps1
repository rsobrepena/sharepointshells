
#==============================================================
# Microsoft provides programming examples for illustration only, 
# without warranty either expressed or implied, including, but not 
# limited to, the implied warranties of merchantability and/or 
# fitness for a particular purpose. 
# This sample assumes that you are familiar with the programming 
# language being demonstrated and the tools used to create and debug 
# procedures. Microsoft support professionals can help explain the 
# functionality of a particular procedure, but they will not modify 
# these examples to provide added functionality or construct 
# procedures to meet your specific needs. if you have limited 
# programming experience, you may want to contact a Microsoft 
# Certified Partner or the Microsoft fee-based consulting line at 
# (800) 936-5200. 
# for more information about Microsoft Certified Partners, please 
# visit the following Microsoft Web site:
# https://partner.microsoft.com/global/30000104 
#
# Author: Tim Quinlan (tim.quinlan@microsoft.com)
#
# ----------------------------------------------------------
# History
# ----------------------------------------------------------
param([string]$computerName,[System.Management.Automation.PsCredential]$farmCred);

#load the global and helper functions
#get the location of the execution of this PS1 file
$path = Split-Path -parent $MyInvocation.MyCommand.Definition
. $path\SharePoint_Helper.ps1
. $path\SharePoint_Globals.ps1


if ( $computerName -like "*app*" )
{
$sb = "Add-PSSnapin Microsoft.SharePoint.PowerShell; `
  Connect-SPConfigurationDatabase -DatabaseServer $databaseServerName -DatabaseName $configDB -Passphrase (ConvertTo-SecureString -string $passphrase -AsPlainText -Force) -SkipRegisterAsDistributedCacheHost;`
  Install-SPHelpCollection -All;Initialize-SPResourceSecurity;Install-SPService;Install-SPFeature -AllExistingFeatures;Install-SPApplicationContent;";
}
else
{
 $sb = "Add-PSSnapin Microsoft.SharePoint.PowerShell; `
Connect-SPConfigurationDatabase -DatabaseServer $databaseServerName -DatabaseName $configDB -Passphrase (ConvertTo-SecureString -string $passphrase -AsPlainText -Force);`
Install-SPHelpCollection -All;Initialize-SPResourceSecurity;Install-SPService;Install-SPFeature -AllExistingFeatures;Install-SPApplicationContent;";
}

$answer = Read-Host "Do you want to join $($computerName) to the current SharePoint farm (y/n)?";
if ( $answer -eq "y" )
{
try
{
Invoke-CommandHelper -computer $computerName -cmd $sb -cred $farmCred;
Write-Log -Level Info  -Message "Ensuring that the Timer Service is started." ;
$sb = "Restart-Service SPTimerv4";
Invoke-CommandHelper -computer $computerName -cmd $sb -cred $farmCred;
Write-Log -Level Info  -Message "Complete" ;
}
catch
{
    Write-Log -Level Warn -Message "An error occurred";
    Write-Log -Level Warn -Message $_ #error details;
}
  
}

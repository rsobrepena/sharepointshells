param([string]$siteUrl,[string]$fileName)

[xml]$termXml = gc $filename;
$termSets = $termXml.TermSets | %{ $_.TermSet }

$site = Get-SPSite $siteUrl;
$tx = new-object Microsoft.SharePoint.Taxonomy.TaxonomySession($site);
$group = $tx.DefaultSiteCollectionTermStore.Groups["AFCENT"]
if($group -eq $null )
{
  Write-Log -Level Info  -Message "Creating Group.." ;
  $group =  $tx.DefaultSiteCollectionTermStore.CreateGroup("AFCENT");

}


$termSets | %{ 


#Create TermSet
 Write-Log -Level Info  -Message "Creating TermSet $($_.name)" ;
 $ts = $group.CreateTermSet($_.name, (new-object System.Guid($_.Id)),1033);
#Create Terms
 $_.Term | %{ 
 Write-Log -Level Info  -Message "Creating Term $($_.name)" ;
 $ts.CreateTerm($_.name, 1033, (new-object System.Guid($_.Id))) | out-null; }
 
}
$tx.DefaultSiteCollectionTermStore.CommitAll();
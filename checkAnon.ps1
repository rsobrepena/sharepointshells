﻿param(

[string] $siteUrl,
[string] $fileLocation,
[string] $fileName

)

function getAnonStatus($myItem){
    
    
    $row = $table.NewRow();
    
    $row.Title = $myItem.Title;
    $row.Type = $myItem.GetType().Name;
    
    $row.AnonMask = $myItem.AnonymousPermMask64.toString();
    

    if($myItem.GetType().Name -eq "SPWeb"){
        $row.AnonState = $myItem.AnonymousState.toString();
        $row.Url = $myItem.Url;
    }else{
        $row.AnonState = "";
        $row.Url = $myItem.DefaultViewUrl;
    }
    
    

    $table.Rows.Add($row);
}

Add-PSSnapin Microsoft.SharePoint.PowerShell

if(!$siteUrl){
    $siteUrl = Read-Host "Enter site collection url";
}

if(!$fileLocation){
    $fileLocation = Read-Host "Enter output location:";
}

if(!$fileName){
    $fileName = Read-Host "Enter output file name:";
}

$table = New-Object System.Data.DataTable "AnonymousPermissionsCheck"

$colTitle = New-Object System.Data.DataColumn Title,([string])
$colUrl = New-Object System.Data.DataColumn Url,([string])
$colType = New-Object System.Data.DataColumn Type,([string])
$colPermMask = New-Object System.Data.DataColumn AnonMask,([string])
$colPermState = New-Object System.Data.DataColumn AnonState,([string])


$table.Columns.Add($colTitle)
$table.Columns.Add($colUrl)
$table.Columns.Add($colType)
$table.Columns.Add($colPermMask)
$table.Columns.Add($colPermState)


$date = Get-Date

if(!$fileName){
    $fileName = "AnonymousPermissionsReport-" + $date.Month + "." + $date.Day + "." + $date.Year + ".csv"
}else{
    if(!$fileName.EndsWith(".csv")){
        $fileName = $fileName + ".csv"
    }

}

if(!$fileLocation.EndsWith("\")){
    $fileLocation = $fileLocation + "\"
}

 
$fileFullPath = $fileLocation + $fileName


Get-SPSite -Limit all | Get-SPWeb -Limit all | %{
    getAnonStatus($_);
    $_.Lists | %{
        getAnonStatus($_);
        }
    }



$table | Export-Csv $fileFullPath
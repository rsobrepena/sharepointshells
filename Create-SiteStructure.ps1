param([switch]$full,[switch]$createDB,[switch]$createSites,[switch]$createWebs,[switch]$createArchive,[switch]$debug)
#load the global and helper functions
#get the location of the execution of this PS1 file
$path = Split-Path -parent $MyInvocation.MyCommand.Definition
. $path\SharePoint_Helper.ps1
. $path\SharePoint_Globals.ps1  

Write-Log -Level Warn -Message "DEBUG is set to $($debug)";

function Enable-Publishing()
{   
  param([string]$url,[switch]$site, [switch]$web)
  
  if($site)
  {
	$newSite = Get-SPSite $url;
	if ($newSite.Features["f6924d36-2fa8-4f0b-b16d-06b7250180fa"] -eq $null )
	{
		Write-Log -Level Info -Message "Activating site Feature on $($newSite.Url)";
		$g = new-object System.Guid("f6924d36-2fa8-4f0b-b16d-06b7250180fa");
		if(!$debug)
		{
			$newSite.Features.Add($g) | out-null;
		}
	}
  }
  if($web)
  {
		$newWeb = Get-SPWeb $url
		if( $newWeb.Features["94c94ca6-b32f-4da9-a9e3-1f3d343d7ecb"] -eq $null )
		{
			Write-Log -Level Info -Message "Activating web Feature on $($_.Url)";
			$g = new-object System.Guid("94c94ca6-b32f-4da9-a9e3-1f3d343d7ecb");
			if(!$debug)
			{
				$newWeb.Features.Add($g) | out-null;
				$newWeb.Update();
			}
		}
		$newWeb.Dispose();
  }
}

function Create-SPSite()
{
	param([string]$url,[string]$title,[string]$template,[string]$owner,[string]$database)

	#check if the site already exists
	$exists = (Get-SPSite $url -EA 0) -ne $null;
	if( !$exists  )
	{
		Write-Log -Level Info  -Message "Begin - Creating Site Collection $($title) at $($url) in database $($database)";
		if(!$debug)
		{
			try
			{
				$newSite = New-SPSite -Url "$($url)" -Template "$($template)" -OwnerAlias $owner `
				-Name "$($title)" -ContentDatabase "$($database)" -EA Stop; 

				#Enable publishing
				Enable-Publishing -url $newSite.Url -site -web;
				Update-ThemeOptions -web $newSite.Url;
			}
			catch
			{
				Write-Log -Level Error -Message "An error occurred creating site collection $($url)";
			}

		}
	}
	else
	{
		Write-Log -Level Warn  -Message "Site Collection $($url) already exists";
	}#endifexists
	 
}

function Create-SPWeb()
{
	param([string]$url,[string]$title,[string]$template)

	#check if the web already exists
	$exists = (Get-SPWeb $url -EA 0) -ne $null;
	if( !$exists  )
	{
		Write-Log -Level Info  -Message "Begin - Creating Web $($title) at $($url)";
		if(!$debug)
		{
			try
			{
				$newWeb = New-SPWeb -Url "$($url)" -Template "$($template)" -Name "$($title)" -EA Stop; 
				Enable-Publishing -url $newWeb.Url -web
				Update-ThemeOptions -web $newWeb.Url
				$newWeb.Dispose();
			}
			catch
			{
				Write-Log -Level Error -Message "An error occurred creating web $($url)";
				$newWeb.Dispose();
			}

		}
	}
	else
	{
		Write-Log -Level Warn  -Message "Web $($url) already exists";
	}#endifexists
	 
}

function Mount-SPDatabase()
{
	param([string]$dName, [string]$application)
	Write-Log -Level Info "Checking if SPContentDatabase $($dName) exists on $($application)";
	$exists = (Get-SPContentDatabase $dName -EA 0) -ne $null;
	if(!$exists)
	{
		Write-Log -Level Info "Mounting SPContentDatabase $($dName) to $($application)";
		if(!$debug)
		{
			if( $dName -notlike "*mysite*" )
			{
		   		Mount-SPContentDatabase -Name $dName -WebApplication $application -MaxSiteCount 1 -WarningSiteCount 0;
			}
			elseif( $dName -like "*PORTAL_HOME*" )
			{
				Mount-SPContentDatabase -Name $dName -WebApplication $application -MaxSiteCount 5 -WarningSiteCount 4;
			}
			elseif( $dName -like "*AFCENT_ARCHIVE*" )
			{
				Mount-SPContentDatabase -Name $dName -WebApplication $application -MaxSiteCount 2 -WarningSiteCount 1;
			}
			else
			{
				Mount-SPContentDatabase -Name $dName -WebApplication $application;
			}
		}
	}
	else
	{
		Write-Log -Level Warn  -Message "SPContentDatabase $($dName) already exists in $($application)";
	}
}

function Get-DBName()
{
	param([string]$coreName,[string]$application)
	if( $network -eq "n")
    {
		if($coreName -eq "")
		{
				#If the database name is blank, place it in the Portal Home DB
				$dbName = "$($farmprefix)_PORTAL_HOME_CONTENT";
		}
		else
		{
			  $dbName = "$($coreName)_CONTENT";
		}
	}#end n
	else
	{
		$sfx = "";
		if($application -like "*.rel.*" ){ $sfx = "REL_" }else{ $sfx="US_" }
		if($coreName -eq "")
		{
				#If the database name is blank, place it in the Portal Home DB
				$dbName = "$($farmprefix)_PORTAL_HOME_$($sfx)CONTENT";
		}
		else
		{
			   $dbName = "$($coreName)_$($sfx)CONTENT";
		}
	}
	Write-Log -Level Info -Message "Determined database name to be $($dbName)";
	return $dbName;
}


function Build()
{
	[xml]$xFile = gc AFCENT_SiteStructure.xml;
	$wa = $webApps | ?{ ($_.ParentApp -eq $null) -and ($_.Name -notlike "*ems*") } | select -ExpandProperty Url
	$wa | %{ $application = $_;

		if($createDB -or $full)
		{
			$databases = $xFile.Webs.Web | select -ExpandProperty database -Unique
			$databases | %{
				#get the full name of the database
				$fullDBName = Get-DBName -coreName $_ -application $application;
				#Mount the database
				Mount-SPDatabase -dName $fullDBName -application $application;

			}#foreachdatabase
			#Mount the My Site DB
			$fullDBName = Get-DBName -coreName "$($farmprefix)_MySite" -application $application;
			#Mount the database
			Mount-SPDatabase -dName $fullDBName -application $application;
			
		}#ifcreateDB
		if($createSites -or $full)
		{
			$siteCols = $xFile.Webs.Web | ?{ $_.siteCol -eq "true" };
			$siteCols | %{
					#determine database name
					$fullDBName = Get-DBName -coreName $_.database -application $application
					#create the site
					Create-SPSite -url "$($application)$($_.Url)" -title $_.title -template "STS#0" -owner $farmaccount -database $fullDBName

			}#foreachsitecol
		}
		if($createWebs -or $full)
		{
			$webs = $xFile.Webs.Web | ?{ (($_.siteCol -eq "false") -and ( $_.Url.Split('/').Count -gt 4)) -or ($_.explicit -eq "true") };
			$webs | %{
				Create-SPWeb -url "$($application)$($_.Url)" -template "STS#0" -title "$($_.Title)"

			}#endforeachwebs
		}
		if($createArchive -or $full)
		{
			$aWebs =$xfile.webs.web | ?{ $_.Url.Split('/').Count -gt 2 } | sort Url
			$archiveSites | %{
				#determine database name
				$fullDBName = Get-DBName -coreName $_.database -application $application
				#Mount the database
				Mount-SPDatabase -dName $fullDBName -application $application;

				#create the site collections
				Create-SPSite -url "$($application)/$($_.Url)" -title "$($_.Name)" -template $_.template -owner $farmaccount -database $fullDBName
			}
			#Build the webs
			$aWebs | %{
				Create-SPWeb -url "$($application)/a$($_.Url)" -template "STS#0" -title "$($_.Title) Archive"
			}#endforeachaweb
		}
	}#end foreach web app

}

Build




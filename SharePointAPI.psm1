﻿Function Filter-OrgSites
{
    [CmdletBinding()]
    Param(
    [Parameter(Mandatory=$True,ValueFromPipeline=$True,ValueFromPipelinebyPropertyName=$True)]
    [Microsoft.SharePoint.PowerShell.SPSitePipeBind[]]$sites
    )

    PROCESS
    {
        Foreach ($site in $sites)
        {
            $spsite = $site.Read();
            
            if(
            ($spsite.Url -notlike "*/help*" ) -and 
            ($spsite.Url -notlike "*sites/*") -and 
            ($spsite.Url -notlike "*personal/*") -and 
            ($spsite.Url -notlike "*/ecoi*") -and 
            ($spsite.Url -notlike "*/lcoi*")   -and 
            ($spsite.Url -notlike "*/a/*") )        
            {
                Write-Output $spsite;
            }
        }
    }
}

Function Filter-OrgWebs
{
    [CmdletBinding()]
    Param(
    [Parameter(Mandatory=$True,ValueFromPipeline=$True,ValueFromPipelinebyPropertyName=$True)]
    [Microsoft.SharePoint.PowerShell.SPWebPipeBind[]]$sites
    )

    PROCESS
    {
        Foreach ($site in $sites)
        {
            $spsite = $site.Read();
            
            if(
            ($spsite.Url -notlike "*/help*" ) -and 
            ($spsite.Url -notlike "*sites/*") -and 
            ($spsite.Url -notlike "*personal/*") -and 
            ($spsite.Url -notlike "*/ecoi*") -and 
            ($spsite.Url -notlike "*/lcoi*")   -and 
            ($spsite.Url -notlike "*/mgt*")   -and 
            ($spsite.Url -notlike "*/search*")   -and
            ($spsite.Url -notlike "*/a/*") )        
            {
                Write-Output $spsite;
            }
        }
    }
}

Function Upload-ListTemplate
{
    [CmdletBinding()]
    Param(
    [Parameter(Mandatory=$True,ValueFromPipeline=$True,ValueFromPipelinebyPropertyName=$True)]
    [Microsoft.SharePoint.PowerShell.SPSitePipeBind[]]$sites,
    [System.IO.FileSystemInfo]$file
    )
    PROCESS
    {
        Foreach ($site in $sites)
        {
            $spsite = $site.Read();
            $web = $spsite.OpenWeb();
            $TemplateGallery = $web.GetFolder("List Template Gallery"); 
            $TemplateFiles = $TemplateGallery.Files; 
            $NewTemplate = $TemplateFiles.Add("_catalogs/lt/$($file.Name)", $file.OpenRead(), $True); 
            Write-Output $NewTemplate; 
        } 
    } 
} 
        
Function Get-SPWebsFromFile 
{ 
    [CmdletBinding()] 
    Param( 
        [Parameter(Mandatory=$True,ValueFromPipeline=$True,ValueFromPipelinebyPropertyName=$True)][string]$fileName
    )

    $file = Get-Content $fileName;
    $file.Name;
    Foreach ($url in $file)
    {
        Get-SPWeb $url
    }
}

Function Get-SPSitesFromFile
{
    [CmdletBinding()]
    Param(
    [Parameter(Mandatory=$True,ValueFromPipeline=$True,ValueFromPipelinebyPropertyName=$True)][string]$fileName
    )

    $file = Get-Content $fileName;
    $file.Name;
    Foreach ($url in $file)
    {
        Get-SPSite $url
    }
}

Function Get-Html
{
    [CmdletBinding()]
    Param(
    [Parameter(Mandatory=$True,ValueFromPipeline=$True,ValueFromPipelinebyPropertyName=$True)][string]$url
    )

    Invoke-WebRequest -Uri $url -UseDefaultCredentials
}

Export-ModuleMember -Function Filter-OrgSites,Filter-OrgWebs, Upload-ListTemplate, Get-SPWebsFromFile, Get-SPSitesFromFile, Get-Html 
